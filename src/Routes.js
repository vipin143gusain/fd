import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import BankDetails from "./BankDetails";
import InvestAmount from "./InvestAmount";
import Faq from "./Faq";
import ConfirmationPersonalDetail from "./ConfirmationPersonalDetail";
import AdditionalDetails from "./AdditionalDetails";
import App from "./App";
import UploadDocs from "./UploadDocuments";
import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import Feedback from "./Feedback";
import Congratulations from "./Congratulations";
import DepositSummary from "./DepositSummary";
import OpenNewFD from "./OpenNewFD.js";
import Test2 from "./Test";
import MyDeposit from "./RepeatJourney/MyDeposit";
import ContactUs from "./RepeatJourney/ContactUs";
import Preconfirmation from "./RepeatJourney/Preconfirmation";
import FDsummary from "./RepeatJourney/FDsummary";
import BreakFD from "./RepeatJourney/BreakFD";
import TaxCorner from "./RepeatJourney/TaxCorner";
import ResumeFd from "./RepeatJourney/ResumeFd";
import MyProfile from "./RepeatJourney/MyProfile";
import Login from "./RepeatJourney/Login";

const Routes = () => {
  const theme = createMuiTheme();
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route path="/my-profile">
            <MyProfile />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/resume-fd">
            <ResumeFd />
          </Route>
          <Route path="/tax-corner">
            <TaxCorner />
          </Route>
          <Route path="/fd-summary/:index">
            <FDsummary />
          </Route>
          <Route path="/break-fd">
            <BreakFD />
          </Route>
          <Route path="/open-new-fd">
            <OpenNewFD />
          </Route>
          <Route path="/preconfirmation">
            <Preconfirmation />
          </Route>
          <Route path="/contact-us">
            <ContactUs />
          </Route>
          <Route path="/my-deposit">
            <MyDeposit />
          </Route>
          <Route path="/test">
            <Test2 />
          </Route>
          <Route path="/deposit-summary/:Cn?">
            <DepositSummary />
          </Route>
          <Route path="/additional-details/:Rj?">
            <AdditionalDetails />
          </Route>
          <Route path="/congratulations/:Cn?">
            <Congratulations />
          </Route>
          <Route path="/feedback">
            <Feedback />
          </Route>
          <Route path="/upload-documents/:mode/:Cn">
            <UploadDocs />
          </Route>
          <Route path="/pancard">
            <App />
          </Route>
          <Route path="/bank-details/:Rj?">
            <BankDetails />
          </Route>
          <Route path="/pancard">
            <App />
          </Route>
          <Route path="/invest-amount/:Cn?/:Rj?">
            <InvestAmount />
          </Route>
          <Route path="/faq">
            <Faq />
          </Route>
          <Route path="/confirmation-personal-detail">
            <ConfirmationPersonalDetail />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
};
export default Routes;
