import React from "react";
import PropTypes from "prop-types";
import Slider from "@material-ui/core/Slider";
import Tooltip from "@material-ui/core/Tooltip";

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

ValueLabelComponent.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired
};

export default function CustomizedSlider(props) {
  return (
    <Slider
      ValueLabelComponent={ValueLabelComponent}
      defaultValue={1}
      min={10000}
      max={1000000}
      step={10000}
      aria-label="custom thumb label"
      onChange={props.handleChange}
    />
  );
}
