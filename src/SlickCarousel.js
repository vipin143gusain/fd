import React from "react";
import Slider from "react-slick";
import "./App.css";

export default class ReactSlickDemo extends React.Component {
  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      centerMode: true
    };
    return (
      <div className="container">
        <Slider {...settings}>
          <div>
            <img src="static/media/congratulationsBg.ea6e1dad.svg" alt="" />
          </div>
          <div>
            <img src="static/media/congratulationsBg.ea6e1dad.svg" alt="" />
          </div>
          <div>
            <img src="static/media/congratulationsBg.ea6e1dad.svg" alt="" />
          </div>
          <div>
            <img src="static/media/congratulationsBg.ea6e1dad.svg" alt="" />
          </div>
        </Slider>
      </div>
    );
  }
}
