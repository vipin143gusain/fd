import React from "react";
import VerifiedIconImg from "./images/verifiedIcon.svg";

export default function VerifiedIcon(props) {
  return (
    <div className="verifiedWrapper">
      <img src={VerifiedIconImg} alt="icon" />
      <p>{props.changeTxt}</p>
    </div>
  );
}
