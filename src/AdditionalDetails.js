import React, { Component } from "react";
import Appbar from "./AppBar";
import ExpansionPanel from "./ExpansionPanel";
// import Button from "@material-ui/core/Button";
export default class AdditionalDetails extends Component {
  render() {
    return (
      <div className="addDetail">
        <Appbar name="Additional Details" />
        <ExpansionPanel />
        {/* <Button
          variant="contained"
          id="addDetailBtn"
          disabled
          className="addDetailBtn"
        >
          Proceed
        </Button> */}
      </div>
    );
  }
}
