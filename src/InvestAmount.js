import React from "react";
import "./App.css";
import AppBar from "./AppBar";
import Grid from "@material-ui/core/Grid";
import Slider from "./Slider";
import InputBase from "@material-ui/core/InputBase";
import { makeStyles, withStyles } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import Box from "@material-ui/core/Box";
import GreenTickIcon from "./images/greenTickIcon.svg";
import { indianMoneyFormat } from "./Common";
import { connect } from "react-redux";
import { withRouter } from "react-router";
// import { investAmountUpdateLead } from "./actions/invest_amout_action";
import {
  updateYear,
  updatePayoutOption,
  updateAmount,
  updateInterestAmount,
  enableEdit,
  updateROI,
  saveLead,
  updateLead,
  updateFDAccount
} from "./actions/calculator_action";

const BootstrapInput = withStyles(theme => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    maxlength: 6,
    pattern: "d*",
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(",")
  }
}))(InputBase);

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1)
    }
  },
  editNum: {
    maxLength: "10",
    type: "number"
  }
}));

const InvestAmount = props => {
  const classes = useStyles();
  console.log("Lead ID", props);

  React.useEffect(
    () => {
      console.log("invest amount-------------------------");
      if (props.calculator.payout_option && props.calculator.year > 0) {
        props.updateROI(
          props.calculator.interesets[props.calculator.payout_option][
            props.calculator.year
          ]
        );
        calculate();
      }
    },
    /* eslint-disable */

    [
      props.calculator.amount,
      props.calculator.year,
      props.calculator.payout_option
    ]
    /* eslint-disable */
  );

  React.useEffect(() => {
    props.saveLead();
  }, []);

  React.useEffect(() => {
    if (
      props.calculator.updatedLead &&
      (!props.match.params.Cn ||
        (props.match.params.Cn && props.match.params.Cn == "nil"))
    ) {
      console.log("update lad ", props.match.params.Rj);
      if (props.match.params.Rj) {
        console.log("repeat journey");
        props.history.push("/additional-details/1", {user : props.location.state.user});
      } else {
        console.log("pancard");
        console.log("invest amount-------------------------");
        props.history.push("/pancard");
      }
    }
  });

  React.useEffect(() => {
    if (props.calculator.updateFdAccount) {
      props.history.push("/deposit-summary/" + props.match.params.Cn);
    }
  });
  //Edit button click
  const handleEnableClick = () => {
    props.enableEdit();
    document.getElementById("demo-customized-textbox").focus();
  };

  //FD Calculator Start
  const calculate1YearCumulativeInterest = effRateOfInt => {
    let interest1 = Math.round(
      (props.calculator.amount * (effRateOfInt / 100)) / 4
    );
    let interest2 = Math.round(
      ((props.calculator.amount + interest1) * (effRateOfInt / 100)) / 4
    );
    let interest3 = Math.round(
      ((props.calculator.amount + interest1 + interest2) *
        (effRateOfInt / 100)) /
        4
    );
    let interest4 = Math.round(
      ((props.calculator.amount + interest1 + interest2 + interest3) *
        (effRateOfInt / 100)) /
        4
    );

    return interest1 + interest2 + interest3 + interest4;
  };

  const calculate2YearCumulativeInterest = effRateOfInt => {
    let year1Interest = calculate1YearCumulativeInterest(effRateOfInt);
    let interest1 = Math.round(
      ((props.calculator.amount + year1Interest) * (effRateOfInt / 100)) / 4
    );
    let interest2 = Math.round(
      ((props.calculator.amount + year1Interest + interest1) *
        (effRateOfInt / 100)) /
        4
    );
    let interest3 = Math.round(
      ((props.calculator.amount + year1Interest + interest1 + interest2) *
        (effRateOfInt / 100)) /
        4
    );
    let interest4 = Math.round(
      ((props.calculator.amount +
        year1Interest +
        interest1 +
        interest2 +
        interest3) *
        (effRateOfInt / 100)) /
        4
    );

    return interest1 + interest2 + interest3 + interest4 + year1Interest;
  };

  const calculate3YearCumulativeInterest = effRateOfInt => {
    let year2Interest = calculate2YearCumulativeInterest(effRateOfInt);
    let interest1 = Math.round(
      ((props.calculator.amount + year2Interest) * (effRateOfInt / 100)) / 4
    );
    let interest2 = Math.round(
      ((props.calculator.amount + year2Interest + interest1) *
        (effRateOfInt / 100)) /
        4
    );
    let interest3 = Math.round(
      ((props.calculator.amount + year2Interest + interest1 + interest2) *
        (effRateOfInt / 100)) /
        4
    );
    let interest4 = Math.round(
      ((props.calculator.amount +
        year2Interest +
        interest1 +
        interest2 +
        interest3) *
        (effRateOfInt / 100)) /
        4
    );

    return interest1 + interest2 + interest3 + interest4 + year2Interest;
  };

  const calculate = () => {
    let roi =
      props.calculator.interesets[props.calculator.payout_option][
        props.calculator.year
      ];
    let rateOfInterest = Number(roi.toFixed(2));
    let effRateOfInt = rateOfInterest + 0.25;
    let finalInterest = 0;

    console.log(effRateOfInt, rateOfInterest);
    if (props.calculator.payout_option === "QP") {
      let interest = Math.round(
        (props.calculator.amount * (effRateOfInt / 100)) / 4
      );
      finalInterest = interest * (4 * props.calculator.year);
    } else if (props.calculator.payout_option === "MP") {
      switch (props.calculator.year) {
        case 1:
          finalInterest = calculate1YearCumulativeInterest(effRateOfInt);
          break;
        case 2:
          finalInterest = calculate2YearCumulativeInterest(effRateOfInt);
          break;
        case 3:
          finalInterest = calculate3YearCumulativeInterest(effRateOfInt);
          break;
        default:
          console.log("Invalid Year");
      }
    }

    props.updateInterestAmount(finalInterest);
  };
  //FD Calculator End

  const proceedClickHandle = () => {
    if (props.calculator.saveLeadId) {
      console.log("saveLeadID");
      let update_data = {
        id: props.calculator.saveLeadId,
        fd_amount: props.calculator.amount,
        maturity_amount:
          Math.abs(props.calculator.amount) +
          Math.abs(props.calculator.total_interest),
        quarter_payout: 0, //static
        roi: props.calculator.roi,
        special: true, //static
        schemeType: "CS", //static
        tenure: props.calculator.year, //static
        maturity_date: "17/02/2021" //static
      };

      if (props.match.params.Cn && props.match.params.Cn != "nil") {
        console.log("update FDD ");
        delete update_data.id;
        console.log("chandan", props.match.params);
        console.log("chandan", props.match.params.Cn);
        update_data.application_no = props.match.params.Cn;
        update_data.type = "customer";
        update_data.tenure = props.calculator.year;
        //update_data.quarter_payout = props.payout_option === "MP" ? 0 : 1;
        update_data.schemeType =
          props.calculator.payout_option === "MP" ? "CS" : "PS";
        props.updateFDAccount(update_data);
      } else {
        console.log("update lead ");
        props.updateLead(update_data);
      }
    }
  };

  //HandleSlider

  const handleSliderChange = (event, val) => {
    props.updateAmount(val);
  };

  let displayAmount = indianMoneyFormat(
    Math.abs(props.calculator.amount) +
      Math.abs(props.calculator.total_interest)
  );

  return (
    <div className="App">
      <AppBar name="Fixed Deposit Calculator" />

      <div className="marTop">
        <div className="orangebg">
          <p>Maturity Amount</p>
          <h1 className="lakhTxt">
            <span>₹ </span>
            <span minLength="5" maxLength="6" size="6" type="number">
              {displayAmount}
            </span>
          </h1>
          <p className="returnTxt">
            {props.calculator.roi.toFixed(2) + "%"} + 0.25% returns / annum
          </p>
        </div>
        <Box className="textWrapper" boxShadow={2}>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <p className="yrDepositTxt">Your Deposit</p>
            </Grid>
            <Grid item xs={6}>
              <div className="numberTxt">
                <span>₹ </span>
                <button
                  className="editIconInvestment"
                  onClick={handleEnableClick}
                >
                  <EditIcon />
                </button>
                <BootstrapInput
                  type="number"
                  className="inputEditBox"
                  id="demo-customized-textbox"
                  value={props.calculator.amount}
                  readOnly={!props.calculator.is_enable}
                  onChange={e => {
                    let amt = e.target.value;

                    if (amt.length > 7) {
                      return false;
                    }
                    console.log("Amt ", amt);
                    if (amt.length !== 0) {
                      props.updateAmount(Number(amt));
                    } else {
                      props.updateAmount(amt);
                    }
                  }}
                />
              </div>
            </Grid>
          </Grid>

          <div className="minMax">
            <Slider handleChange={handleSliderChange} />
            <p>
              <span className="tenK">₹10K</span>
              <span className="tenLacs">₹10 Lacs</span>
            </p>
            <div className="clearfix posRel">
              <span className="minTxt">min</span>
              <span className="maxTxt">max</span>
            </div>
          </div>
        </Box>

        <div className="cumulativeBtns">
          <div className="cumulativeWrapper">
            <p className="payout">Payouts</p>
            {/* <p className="MandatoryField">* Mandatory Field</p> */}

            <div className={classes.root}>
              <Button
                className={
                  "marLeftZero" +
                  (props.calculator.payout_option === "MP"
                    ? " activeCalculatorBtn"
                    : "")
                }
                variant="contained"
                color="primary"
                onClick={() => props.updatePayoutOption("MP")}
              >
                <p className="strongTxt">Cumulative </p>
                <p className="hoverOnly2">Payout on maturity</p>
              </Button>

              <Button
                variant="contained"
                color="primary"
                className={
                  "floatRite" +
                  (props.calculator.payout_option === "QP"
                    ? " activeCalculatorBtn"
                    : "")
                }
                onClick={() => props.updatePayoutOption("QP")}
              >
                <p className="strongTxt">Non-Cumulative </p>
                <p className="hoverOnly2">Quarterly payout</p>
              </Button>
            </div>
          </div>

          <div className="tickIconWrapper">
            <img className="greenTickIcon" src={GreenTickIcon} alt="tick" />
            <span className="checkboxFltLft">
              Additional 0.25% interest
              <span className="selectedTxt"> (For Future Pay Selected)</span>
            </span>
          </div>
        </div>
        <div className="interestBtns">
          <div>
            <p className="payout">Duration</p>
            <div className={classes.root}>
              <Button
                className={
                  "marLeftZero" +
                  (props.calculator.year === 1 ? " activeYearBtn" : "")
                }
                variant="contained"
                color="primary"
                onClick={() => {
                  props.updateYear(1);
                }}
                disabled={!props.calculator.payout_option}
              >
                <p className="strongTxt">1 Y </p>
                <p className="hoverOnly">
                  {props.calculator.payout_option &&
                    props.calculator.interesets[props.calculator.payout_option][
                      "1"
                    ].toFixed(2) + "%"}
                </p>
              </Button>
              <Button
                className={props.calculator.year === 2 ? " activeYearBtn" : ""}
                variant="contained"
                color="primary"
                onClick={() => {
                  props.updateYear(2);
                }}
                disabled={!props.calculator.payout_option}
              >
                <p className="strongTxt">2 Y </p>
                <p className="hoverOnly">
                  {props.calculator.payout_option &&
                    props.calculator.interesets[props.calculator.payout_option][
                      "2"
                    ].toFixed(2) + "%"}
                </p>
              </Button>

              <Button
                className={props.calculator.year === 3 ? " activeYearBtn" : ""}
                variant="contained"
                color="primary"
                onClick={() => {
                  props.updateYear(3);
                }}
                disabled={!props.calculator.payout_option}
              >
                <p className="strongTxt">3 Y </p>
                <p className="hoverOnly">
                  {props.calculator.payout_option &&
                    props.calculator.interesets[props.calculator.payout_option][
                      "3"
                    ].toFixed(2) + "%"}
                </p>
              </Button>
            </div>
          </div>
        </div>

        <div className="greyBg">
          <Button
            variant="contained"
            color="primary"
            className="proceedButtonDisable"
            onClick={proceedClickHandle}
          >
            Proceed
          </Button>
        </div>
      </div>
    </div>
  );
};

function mapStateToProps(store) {
  console.log("store", store.calculator);
  return {
    calculator: store.calculator,
    investAmountDetails: store.investAmountDetails
  };
}

const mapActionToProps = {
  updateYear,
  updatePayoutOption,
  updateAmount,
  updateInterestAmount,
  enableEdit,
  updateROI,
  saveLead,
  updateLead,
  updateFDAccount
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(InvestAmount));
