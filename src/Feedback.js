import React, { Component } from "react";
import AppBar from "./AppBar";
import Button from "@material-ui/core/Button";
import { purple } from "@material-ui/core/colors";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import ShareIcon from "@material-ui/icons/Share";
import Grid from "@material-ui/core/Grid";

const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: "#7d6498",
    "&:hover": {
      backgroundColor: "#7d6498"
    },
    margin: "0px",
    width: "100%",
    fontSize: "11px",
    fontFamily: "Roboto",
    fontWeight: "500",
    padding: "10px"
  }
}))(Button);

export default class Feedback extends Component {
  render() {
    return (
      <div className="feedback">
        <AppBar close />
        <div className="FBwrapper">
          <p>Enjoying FutureMoney?</p>
          <div className="whiteBg">
            <p>How did you like the FD buying experience?</p>
            <div className="emoji">
              {/* <CustomizedRatings /> */}
              <Grid container spacing={0}>
                <Grid item xs={2} className="emojiOne"></Grid>
                <Grid item xs={2} className="emojiTwo"></Grid>
                <Grid item xs={2} className="emojiThree"></Grid>
                <Grid item xs={2} className="emojiFour"></Grid>
                <Grid item xs={2} className="emojiFive"></Grid>
              </Grid>
              {/* <div className="emojiOne"></div>
              <div className="emojiOne"></div>
              <div className="emojiOne"></div>
              <div className="emojiOne"></div>
              <div className="emojiOne"></div> */}
            </div>
            <strong>Sorry for the bad experience.</strong>
            <p className="improveTxt">
              Tell us what you disliked & help us improve.
            </p>

            <div className="shareWrapper">
              <Button variant="contained" disabled color="primary">
                Share <ShareIcon />
              </Button>

              <Button
                variant="contained"
                disabled
                color="primary"
                className="submitBtn"
              >
                Rate us on PlayStore
              </Button>
            </div>

            <div className="chip">
              <ColorButton
                variant="contained"
                color="primary"
                className="chipBtn"
              >
                Payment
              </ColorButton>

              <ColorButton
                variant="contained"
                color="primary"
                className="chipBtn"
              >
                Document Upload
              </ColorButton>

              <ColorButton
                variant="contained"
                color="primary"
                className="chipBtn"
              >
                Email Verification
              </ColorButton>

              <ColorButton
                variant="contained"
                color="primary"
                className="chipBtn"
              >
                Account Verification
              </ColorButton>

              <ColorButton
                variant="contained"
                color="primary"
                className="chipBtn"
              >
                Look & Feel
              </ColorButton>

              <ColorButton
                variant="contained"
                color="primary"
                className="chipBtn"
              >
                Others
              </ColorButton>

              <TextField
                className="remarksBox"
                id="filled-multiline-static"
                label="Remarks (Optional)"
                multiline
                rows="4"
                variant="filled"
              />
            </div>
          </div>
          <div>
            <Button
              className="feedbackSubmitBtn"
              variant="contained"
              color="primary"
            >
              Submit
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
