import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Appbar from "./AppBar";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import GenerateOtpBtn from "./Modal";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import {
  sendEmailOTP,
  verifyEmailOTP
} from "./actions/personal_details_action";
import {
  updateField,
  getStateList,
  getCityList,
  setStateName,
  setCityName
} from "./actions/pancard_action";
// eslint-disable-next-line no-useless-escape
const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: "65px"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  margin: {
    margin: theme.spacing(1)
  }
}));

let additionalFormValues = false;

const ConfirmationPersonalDetails = props => {
  console.log("props", props);
  const classes = useStyles();
  const [stateName, setStateName] = React.useState("");
  /* eslint-disable */
  const [cityName, setCityName] = React.useState("");

  React.useEffect(() => {
    props.getStateList();
  }, []);

  React.useEffect(() => {
    if (props.getStateName) {
      console.log("callll");
      document.getElementById("State").selectedIndex = props.getStateName;
    }
  }, []);

  React.useEffect(() => {
    console.log("effect Props", props);
    if (
      props.addr_line1 &&
      props.addr_line2 &&
      props.pincode &&
      props.getStateName &&
      props.getCityName
    ) {
      additionalFormValues = true;
      setStateName(props.getCityName);
    }
  });

  const handleStateChange = event => {
    console.log(event.target.value);
    props.getCityList(event.target.value);
    setStateName(event.target.value);
    props.setStateName(event.target.value);
    handleAddressValidation();
  };

  const handleCityChange = event => {
    setCityName(event.target.value);
    console.log("vipig", event.target.value);
    props.setCityName(event.target.value);
    handleAddressValidation();
  };

  const sendOTP = () => {
    console.log("Test Email", props.email);
    props.sendEmailOTP("email", props.email);
  };
  const handeVerifyOTP = () => {
    props.verifyEmailOTP(props.email, props.otp);
  };

  const hanldeOTPText = otp => {
    if (otp.length <= 6) {
      props.updateField("otp", otp);
    }
  };
  const handleAddressValidation = () => {
    console.log("handleAddressValidation before", props);
    console.log("handleAddressValidation after", props);
    console.log(
      "--",
      props.addr_line1.length,
      props.addr_line2.length,
      props.pincode.length,
      props.getCityName,
      props.getStateName
    );
    if (
      props.addr_line1.length > 0 &&
      props.addr_line2.length > 0 &&
      props.pincode.length >= 5 &&
      props.getCityName &&
      props.getStateName
    ) {
      props.updateField("proceed_to_additional", true);
    } else {
      props.updateField("proceed_to_additional", false);
    }
  };
  const handleProceed = () => {
    props.history.push("/additional-details");
  };

  function callStateList() {
    return (
      props.stateList &&
      props.stateList.map(option => {
        return (
          <option
            key={option.stateCode}
            defaultValue={option.stateCode === stateName}
            value={option.stateCode}
          >
            {option.stateName}
          </option>
        );
      })
    );
  }

  function callCityList() {
    return (
      props.cityList &&
      props.cityList.map(option => {
        return (
          <option
            key={option.cityCode}
            selected={option.cityCode === props.getCityName}
            value={option.cityCode}
          >
            {option.cityName}
          </option>
        );
      })
    );
  }

  return (
    <div className="ConfimationDetailsWrapper">
      <Appbar name="Confirmation Personal Details" />
      <div className={classes.root}>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <CheckCircleIcon className="checkIcon" />
              <p className="panVerfiedTxt">PAN Verified from NSDL</p>
            </Paper>
          </Grid>
          <Grid item xs={6} sm={6}>
            <Paper className={classes.paper}>
              <TextField
                autoComplete="off"
                id="standard-read-only-input"
                label="Name"
                defaultValue={props.first_name + " " + props.last_name}
                InputProps={{
                  readOnly: true
                }}
              />
            </Paper>
          </Grid>
          <Grid item xs={6} sm={6}>
            <Paper className={classes.paper}>
              <TextField
                autoComplete="off"
                id="standard-read-only-input"
                label="Date Of Birth"
                defaultValue={props.dob}
                InputProps={{
                  readOnly: true
                }}
              />
            </Paper>
          </Grid>
          <div className="hrLine"></div>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <p className="verifyTxt">Verify e-mail ID</p>
              <FormControl>
                <TextField
                  autoComplete="off"
                  // autoCapitalize="off"
                  className="emailInput"
                  id="filled-search"
                  label="e-mail address"
                  type="email"
                  variant="filled"
                  onChange={e => {
                    let email = e.target.value;
                    if (emailRegx.test(String(email).toLowerCase())) {
                      props.updateField("is_valid_email", true);
                    } else {
                      props.updateField("is_valid_email", false);
                    }
                    props.updateField("email", email);
                  }}
                  value={props.email}
                />
              </FormControl>
            </Paper>
          </Grid>

          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <GenerateOtpBtn
                sendOTP={sendOTP}
                para={props.email}
                mobileText="email"
                verifyOTP={handeVerifyOTP}
                hanldeOTPText={hanldeOTPText}
                openModal={props.is_otp_verified ? false : props.openModal}
                invalidOTP={props.invalid_otp}
                isValidPhone={props.is_valid_email}
                disabled={props.is_otp_verified}
              />
            </Paper>
          </Grid>

          {props.is_otp_verified && (
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <CheckCircleIcon className="checkIcon" />
                <p className="panVerfiedTxt">Email Verified</p>
              </Paper>
            </Grid>
          )}

          <div className="hrLine2"></div>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <p className="verifyTxt">Address (as per Address Proof)</p>
              <TextField
                autoComplete="off"
                className="emailInput"
                id="filled-search"
                label="Enter address (Line 1)"
                type="Enter address (Line 1)"
                variant="filled"
                value={props.addr_line1}
                onChange={e => {
                  let line1 = e.target.value;
                  props.updateField("addr_line1", line1);
                  handleAddressValidation();
                }}
              />
            </Paper>
          </Grid>

          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <TextField
                autoComplete="off"
                className="emailInput"
                id="filled-search"
                label="Locality, Street (Line 2)"
                type="Locality, Street (Line 2)"
                variant="filled"
                value={props.addr_line2}
                onChange={e => {
                  let line2 = e.target.value;
                  props.updateField("addr_line2", line2);
                  handleAddressValidation();
                }}
              />
            </Paper>
          </Grid>

          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <TextField
                autoComplete="off"
                className="emailInput"
                id="filled-search"
                label="PINCODE"
                type="PINCODE"
                variant="filled"
                value={props.pincode}
                onChange={e => {
                  let pincode = e.target.value;
                  props.updateField("pincode", pincode);
                  handleAddressValidation();
                }}
              />
            </Paper>
          </Grid>

          <Grid item xs={6} sm={6} className="padTopBtmNone">
            <Paper className={classes.paper}>
              <FormControl className={classes.formControl}>
                <label
                  autoComplete="off"
                  className="labelTxt custom-select"
                  for="State"
                  labelId="demo-simple-select-placeholder-label-label"
                  id="demo-simple-select-placeholder-label"
                >
                  State
                </label>
                <select id="State" onChange={handleStateChange}>
                  <option>--Select State--</option>
                  {callStateList()}
                </select>

                {/* <InputLabel
                  shrink
                  id="demo-simple-select-placeholder-label-label"
                >
                  State
                </InputLabel> */}
                {/* <Select
                  value="9"
                  labelId="demo-simple-select-placeholder-label-label"
                  id="demo-simple-select-placeholder-label"
                  onChange={handleStateChange}
                  displayEmpty
                  className={classes.selectEmpty}
                >
                  {callStateList()}
                </Select> */}
              </FormControl>
            </Paper>
          </Grid>
          {/* <Grid item xs={1} sm={1} className="padTopBtmNone">
            <Paper className={classes.paper}>
              <div className="verticalLine"></div>
            </Paper>
          </Grid> */}
          <Grid item xs={6} sm={6} className="padTopBtmNone">
            <Paper className={classes.paper}>
              <FormControl className={classes.formControl}>
                <label autoComplete="off" className="labelTxt" for="City">
                  City
                </label>

                <select
                  autoComplete="off"
                  onChange={handleCityChange}
                  id="City"
                >
                  <option>--Select City--</option>
                  {callCityList()}>
                </select>
                {/* <InputLabel
                  shrink
                  id="demo-simple-select-placeholder-label-label"
                >
                  City
                </InputLabel>
                <Select
                  labelId="demo-simple-select-placeholder-label-label"
                  id="demo-simple-select-placeholder-label"
                  onChange={handleChange}
                  displayEmpty
                  className={classes.selectEmpty}
                >
                  {callCityList()}
                </Select> */}
              </FormControl>
            </Paper>
          </Grid>
        </Grid>
        {console.log("A", props)}
        {console.log("B", props.is_otp_verified)}
        <div className="greyBg marTop">
          <Button
            variant="contained"
            color="primary"
            className="proceedButton"
            disabled={!additionalFormValues || !props.is_otp_verified}
            onClick={handleProceed}
          >
            Proceed
          </Button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ pancard }) => ({
  ...pancard
});

const mapActionToProps = {
  sendEmailOTP,
  updateField,
  verifyEmailOTP,
  getStateList,
  getCityList,
  setStateName,
  setCityName
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(ConfirmationPersonalDetails));
