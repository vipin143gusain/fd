import React from "react";
import "./App.css";
import AppBar from "./AppBar";
import Grid from "@material-ui/core/Grid";
import Slider from "./Slider";

import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import Box from "@material-ui/core/Box";
import GreenTickIcon from "./images/greenTickIcon.svg";

import { makeStyles } from "@material-ui/core/styles";

const editFunc = () => {
  document.getElementById("myP").contentEditable = true;
};
const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1)
    }
  },
  editNum: {
    maxlength: "10",
    type: "number"
  }
}));

export default function InvestAmount() {
  const classes = useStyles();

  return (
    <div className="App">
      <AppBar name="Fixed Deposit Calculator" />

      <div className="marTop">
        <div className="orangebg">
          <p>Maturity Amount</p>
          <h1 className="lakhTxt">
            <span>₹ </span>
            <span maxlength="10" type="number">
              1,21,300
            </span>
          </h1>
          <p className="returnTxt">@10.15% + 0.25% returns / annum</p>
        </div>
        <Box className="textWrapper" boxShadow={2}>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <p className="yrDepositTxt">Your Deposit</p>
            </Grid>
            <Grid item xs={6}>
              <p className="numberTxt">
                <span>₹ </span>
                <span
                  id="myP"
                  maxlength="10"
                  type="number"
                  className={classes.editNum}
                >
                  1,00,000
                </span>
                <button className="editIcon" onClick={editFunc}>
                  <EditIcon />
                </button>
              </p>
            </Grid>
          </Grid>

          <div className="minMax">
            <Slider />
            <p>
              <span className="tenK">₹10K</span>
              <span className="tenLacs">₹10 Lacs</span>
            </p>
            <div className="clearfix posRel">
              <span className="minTxt">min</span>
              <span className="maxTxt">max</span>
            </div>
          </div>
        </Box>

        <div className="interestBtns">
          <div>
            <p className="payout">Duration</p>
            <div className={classes.root}>
              <Button
                className="marLeftZero"
                variant="contained"
                color="primary"
              >
                <p className="strongTxt">1 Y </p>
                <p className="hoverOnly">9.10%</p>
              </Button>
              <Button variant="contained" color="primary">
                <p className="strongTxt">1 Y </p>
                <p className="hoverOnly">9.10%</p>
              </Button>

              <Button variant="contained" color="primary">
                <p className="strongTxt">1 Y </p>
                <p className="hoverOnly">9.10%</p>
              </Button>
            </div>
          </div>
        </div>

        <div className="cumulativeBtns">
          <div className="tickIconWrapper">
            <img className="greenTickIcon" src={GreenTickIcon} alt="tick" />
            <span className="checkboxFltLft">
              Additional 0.25% interest
              <span className="selectedTxt"> (For Future Pay Selected)</span>
            </span>
          </div>

          <div className="cumulativeWrapper">
            <p className="payout">Payouts</p>
            <p className="MandatoryField">* Mandatory Field</p>

            <div className={classes.root}>
              <Button
                className="marLeftZero"
                variant="contained"
                color="primary"
              >
                <p className="strongTxt">Cumulative </p>
                <p className="hoverOnly2">Payout on maturity</p>
              </Button>
            </div>

            <div className={classes.root}>
              <Button variant="contained" color="primary" className="floatRite">
                <p className="strongTxt">Non-Cumulative </p>
                <p className="hoverOnly2">Quarterly payout</p>
              </Button>
            </div>
          </div>
        </div>

        <div className="greyBg">
          <Button
            variant="contained"
            color="primary"
            className="proceedButtonDisable"
          >
            Proceed
          </Button>
        </div>
      </div>
    </div>
  );
}
