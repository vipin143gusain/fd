import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import MenuItems from "./MenuItems";
import {
  sendEmailOTP,
  verifyEmailOTP,
  clearVerifiedOTP,
  addNomineeForm,
  fdMinorForm,
  JointFdForm,
  JointFdForm2,
  saveOptionalDetails
} from "./actions/personal_details_action";
import { updateField } from "./actions/pancard_action";
import { getNextAppNo } from "./actions/bank_details_action";

import IndeterminateCheckBoxOutlinedIcon from "@material-ui/icons/IndeterminateCheckBoxOutlined";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { purple } from "@material-ui/core/colors";
import { withStyles } from "@material-ui/core/styles";

import AddDetailCalender from "./AddDetailsCalender";
import Modal from "./Modal";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";

import { withRouter } from "react-router";
import { connect } from "react-redux";
import {
  enableAddNomineeForm,
  enableFdMinorForm,
  enableJointFdForm
} from "./actions/additional_details_action";

const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: "#7d6498",
    "&:hover": {
      backgroundColor: "#7d6498"
    },
    margin: "0px",
    width: "100%",
    fontSize: "11px",
    fontFamily: "Roboto",
    fontWeight: "500",
    padding: "10px"
  }
}))(Button);

const currencies = [
  {
    value: "USD",
    label: "Father"
  },
  {
    value: "USD1",
    label: "Mother"
  },
  {
    value: "EUR",
    label: "Son"
  },
  {
    value: "BTC",
    label: "Daughter"
  },
  {
    value: "JPY23",
    label: "Husband"
  },
  {
    value: "JPY",
    label: "Brother"
  },
  {
    value: "JPY3",
    label: "Sister"
  },
  {
    value: "JPY4",
    label: "Son-In-Law"
  },
  {
    value: "JPY5",
    label: "Daughter-In-Law"
  },
  {
    value: "JPY6",
    label: "Granddaughter"
  },
  {
    value: "JPY7",
    label: "Mother-In-Law"
  },
  {
    value: "JPY8",
    label: "Others"
  }
];

class SimpleFade extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showMe: [false],
      addNomineeYes: "buttonYesNo",
      addNomineeNo: "buttonYesNoActive",
      fdMinorYes: "buttonYesNo",
      fdMinorNo: "buttonYesNoActive",
      jointFdYes: "buttonYesNo",
      jointFdNo: "buttonYesNoActive",
      isNomineeMinorYes: "buttonYesNo",
      isNomineeMinorNo: "buttonYesNoActive",
      addNomineeFormDetect: false,
      isNomineeMinor: "no",
      minorFormDetect: false,
      formIndex: false,
      joinFdFormDetect: false,
      showAddMoreJoinee: false,
      joinFdFormDetect2: false,
      enableJoinHolderForm: false,
      enableJoinHolderForm2: false
    };
  }

  sendOTP = (form = "form1") => {
    this.props.sendEmailOTP(
      "mobile",
      form === "form2"
        ? document.getElementById("joint-holder-mobile_2")
          ? document.getElementById("joint-holder-mobile_2").value
          : document.getElementById("joint-holder-mobile_1").value
        : document.getElementById("joint-holder-mobile_1").value
    );
  };
  handeVerifyOTP = (form = "form1") => {
    this.props.verifyEmailOTP(
      form === "form2"
        ? document.getElementById("joint-holder-mobile_2")
          ? document.getElementById("joint-holder-mobile_2").value
          : document.getElementById("joint-holder-mobile_1").value
        : document.getElementById("joint-holder-mobile_1").value,
      document.getElementById("standard-basic").value,
      form
    );
  };

  hanldeOTPText = otp => {
    this.props.updateField("otp", otp);
  };

  componentDidMount() {
    //Call all three forms action - set as No
    this.props.enableAddNomineeForm(true);
    this.props.enableFdMinorForm(true);
    this.props.enableJointFdForm(true);
    let dataApp = {
      type: "customer"
    };
    this.props.getNextAppNo(dataApp);

    if (
      this.props.personalDetails &&
      this.props.personalDetails.addNomineeForm &&
      Object.keys(this.props.personalDetails.addNomineeForm).length > 0
    ) {
      this.onclickBtn(1, true);
      if (this.props.personalDetails.addNomineeForm.nomineeMinor === "yes") {
        this.onclickIsNomineeMinor(true);
      } else {
        this.onclickIsNomineeMinor(false);
      }
      this.props.enableAddNomineeForm(true);
    }

    if (
      this.props.personalDetails &&
      this.props.personalDetails.fdMinorForm &&
      Object.keys(this.props.personalDetails.fdMinorForm).length > 0
    ) {
      console.log("1-------------", true);
      this.onclickBtn(2, true);
      this.props.enableFdMinorForm(true);
    }

    if (
      this.props.personalDetails &&
      this.props.personalDetails.jointFdForm &&
      Object.keys(this.props.personalDetails.jointFdForm).length > 0
    ) {
      this.onclickBtn(3, true);
      this.props.enableJointFdForm(true);
    }

    if (
      this.props.personalDetails &&
      this.props.personalDetails.jointFdForm &&
      Object.keys(this.props.personalDetails.jointFdForm2).length > 0
    ) {
      this.addMoreJoinee(true);
    }
  }

  componentDidUpdate() {
    console.log("Did Update", this.props.personalDetails);

    if (this.props.additionalDetails) {
      let element = document.getElementById("addDetailBtn");
      //If All three form true, enable button
      if (
        this.props.additionalDetails.enableNomineeForm &&
        this.props.additionalDetails.enableMinorFdForm &&
        this.props.additionalDetails.enableJointFdForm
      ) {
        console.log("if--------");
        element.removeAttribute("disabled");
        element.classList.remove("addDetailBtnDisable");
        element.classList.add("addDetailBtn");
      } else {
        console.log("else--------");
        //If any one false, disable button
        element.setAttribute("disabled", "true");
        element.classList.add("addDetailBtnDisable");
        element.classList.remove("addDetailBtn");
      }
    }

    if (this.props.personalDetails) {
      if (
        !this.state.enableJoinHolderForm &&
        this.props.personalDetails.verified_otp_form1 === true &&
        !this.state.joinFdFormDetect2
      ) {
        this.props.enableJointFdForm(true);
        this.setState({
          enableJoinHolderForm: true
        });
      }

      if (
        !this.state.enableJoinHolderForm2 &&
        this.props.personalDetails.verified_otp_form2 === true &&
        this.state.joinFdFormDetect2 &&
        this.props.personalDetails.verified_otp_form1 === true
      ) {
        this.props.enableJointFdForm(true);
        this.setState({
          enableJoinHolderForm2: true
        });
      }
    }
  }

  //Onclick - Forms - Yes No
  onclickBtn(index, value) {
    if (index === 1) {
      if (value) {
        this.setState({
          addNomineeYes: "buttonYesNoActive",
          addNomineeNo: "buttonYesNo"
        });
        this.props.enableAddNomineeForm(false);
      } else {
        this.setState({
          addNomineeYes: "buttonYesNo",
          addNomineeNo: "buttonYesNoActive"
        });
        this.props.addNomineeForm({});
        this.props.enableAddNomineeForm(true);
      }
    } else if (index === 2) {
      if (value) {
        this.setState({
          fdMinorYes: "buttonYesNoActive",
          fdMinorNo: "buttonYesNo"
        });
        console.log("2-------------", false);
        this.props.enableFdMinorForm(false);
      } else {
        this.setState({
          fdMinorYes: "buttonYesNo",
          fdMinorNo: "buttonYesNoActive"
        });
        this.props.fdMinorForm({});
        console.log("3-------------", true);
        this.props.enableFdMinorForm(true);
      }
    } else if (index === 3) {
      if (value) {
        this.setState({
          jointFdYes: "buttonYesNoActive",
          jointFdNo: "buttonYesNo"
        });
        this.props.enableJointFdForm(false);
      } else {
        this.setState({
          jointFdYes: "buttonYesNo",
          jointFdNo: "buttonYesNoActive"
        });
        this.props.JointFdForm({});
        this.props.JointFdForm2({});
        this.props.enableJointFdForm(true);
        this.addMoreJoinee(false);
      }
    }

    const newState = this.state.showMe;
    // if (value === true) {
    //   for (var i = 1; i < newState.length; i++) {
    //     newState[i] = true;
    //   }
    // }
    newState[index] = value;
    this.setState({
      showMe: newState
    });
  }

  //Is Nominee Minor - Yes No
  onclickIsNomineeMinor(value) {
    this.setState({ addNomineeFormDetect: false });
    if (value) {
      this.setState({
        isNomineeMinorYes: "buttonYesNoActive",
        isNomineeMinorNo: "buttonYesNo",
        isNomineeMinor: "yes"
      });

      this.props.enableAddNomineeForm(false);
    } else {
      this.setState(
        {
          isNomineeMinorYes: "buttonYesNo",
          isNomineeMinorNo: "buttonYesNoActive",
          isNomineeMinor: "no"
        },
        () => this.onChangeAddNomineeForm()
      );
      this.props.enableAddNomineeForm(true);
    }
  }

  //OnChange Add Nominee Form
  onChangeAddNomineeForm = () => {
    let data = {};
    if (
      document.getElementById("nominee-firstname").value !== "" &&
      document.getElementById("nominee-lastname").value !== "" &&
      document.getElementById("nominee-firstname").value.length > 2 &&
      document.getElementById("nominee-lastname").value.length > 2
    ) {
      if (this.state.isNomineeMinor === "no") {
        let dataNo = {
          firstName: document.getElementById("nominee-firstname").value,
          lastName: document.getElementById("nominee-lastname").value,
          nomineeMinor: this.state.isNomineeMinor
        };
        this.props.addNomineeForm(dataNo);
        //is the nominee a minor - no
        this.props.enableAddNomineeForm(true);
      } else if (this.state.isNomineeMinor === "yes") {
        //is the nominee a minor - no
        if (
          document.getElementById("nominee-guardian").value !== "" &&
          document.getElementById("nominee-guardian").value.length > 2
        ) {
          let dataYes = {
            firstName: document.getElementById("nominee-firstname").value,
            lastName: document.getElementById("nominee-lastname").value,
            nomineeMinor: this.state.isNomineeMinor,
            guardianName: document.getElementById("nominee-guardian").value
          };
          this.props.addNomineeForm(dataYes);
          this.props.enableAddNomineeForm(true);
        } else {
          this.props.enableAddNomineeForm(false);
        }
      } else {
        this.props.enableAddNomineeForm(false);
      }
    } else {
      this.props.addNomineeForm(data);
      this.props.enableAddNomineeForm(false);
    }
  };

  //On Change FD Minor Form
  onChangeForm = () => {
    let dataFd = {};
    if (
      document.getElementById("minor-firstName") &&
      document.getElementById("minor-lastName") &&
      document.getElementById("minor-firstName").value !== "" &&
      document.getElementById("minor-lastName").value !== "" &&
      document.getElementById("minor-firstName").value.length > 2 &&
      document.getElementById("minor-lastName").value.length > 2 &&
      !this.state.minorFormDetect
    ) {
      dataFd = {
        minorFirstName: document.getElementById("minor-firstName").value,
        minorLastName: document.getElementById("minor-lastName").value,
        minorMiddleName: document.getElementById("minor-middleName").value,
        minorDOB: document.getElementById("date").value
      };
      console.log("4-------------", true);
      this.props.enableFdMinorForm(true);
      this.setState({ minorFormDetect: true });
      this.props.fdMinorForm(dataFd);
    } else if (
      document.getElementById("minor-firstName") &&
      document.getElementById("minor-lastName") &&
      document.getElementById("minor-firstName").value !== "" &&
      document.getElementById("minor-lastName").value !== "" &&
      (document.getElementById("minor-firstName").value.length <= 2 ||
        document.getElementById("minor-lastName").value.length <= 2) &&
      this.state.minorFormDetect
    ) {
      this.props.fdMinorForm(dataFd);
      this.setState({ minorFormDetect: false });
      console.log("5-------------", false);
      this.props.enableFdMinorForm(false);
    } else if (
      document.getElementById("minor-middleName") &&
      document.getElementById("minor-middleName")
    ) {
      console.log("In");
      dataFd = {
        minorFirstName: document.getElementById("minor-firstName").value,
        minorLastName: document.getElementById("minor-lastName").value,
        minorMiddleName: document.getElementById("minor-middleName").value,
        minorDOB: document.getElementById("date").value
      };
      this.props.fdMinorForm(dataFd);
    }
  };

  //On Change Joint FD Form
  onChangeJointFdForm = () => {
    let jointData = {};

    if (
      document.getElementById("joint-holder-firstname_1") &&
      document.getElementById("joint-holder-lastname_1") &&
      document.getElementById("joint-holder-pannumber_1") &&
      document.getElementById("joint-holder-mobile_1") &&
      document.getElementById("joint-holder-firstname_1").value !== "" &&
      document.getElementById("joint-holder-lastname_1").value !== "" &&
      document.getElementById("joint-holder-pannumber_1").value !== "" &&
      document.getElementById("joint-holder-mobile_1").value !== "" &&
      document.getElementById("joint-holder-firstname_1").value.length > 2 &&
      document.getElementById("joint-holder-lastname_1").value.length > 2 &&
      document.getElementById("joint-holder-pannumber_1").value.length > 2 &&
      document.getElementById("joint-holder-mobile_1").value.length > 9 &&
      !this.state.joinFdFormDetect
    ) {
      jointData = {
        holderFirstName: document.getElementById("joint-holder-firstname_1")
          .value,
        holderLastName: document.getElementById("joint-holder-lastname_1")
          .value,
        holderPanNumber: document.getElementById("joint-holder-pannumber_1")
          .value,
        holderMobile: document.getElementById("joint-holder-mobile_1").value
      };
      this.props.JointFdForm(jointData);
      this.setState({ joinFdFormDetect: true });
    } else if (
      document.getElementById("joint-holder-firstname_1") &&
      document.getElementById("joint-holder-lastname_1") &&
      document.getElementById("joint-holder-pannumber_1") &&
      document.getElementById("joint-holder-mobile_1") &&
      (document.getElementById("joint-holder-firstname_1").value.length <= 2 ||
        document.getElementById("joint-holder-lastname_1").value.length <= 2 ||
        document.getElementById("joint-holder-pannumber_1").value.length <= 2 ||
        document.getElementById("joint-holder-mobile_1").value.length <= 9) &&
      this.state.joinFdFormDetect
    ) {
      this.props.JointFdForm(jointData);
      this.setState({ joinFdFormDetect: false });
    } else {
      this.props.enableJointFdForm(false);
    }
  };

  //On Change Joint FD Form
  onChangeJointFdForm2 = () => {
    let jointData2 = {};

    if (
      document.getElementById("joint-holder-firstname_2") &&
      document.getElementById("joint-holder-lastname_2") &&
      document.getElementById("joint-holder-pannumber_2") &&
      document.getElementById("joint-holder-mobile_2") &&
      document.getElementById("joint-holder-firstname_2").value !== "" &&
      document.getElementById("joint-holder-lastname_2").value !== "" &&
      document.getElementById("joint-holder-pannumber_2").value !== "" &&
      document.getElementById("joint-holder-mobile_2").value !== "" &&
      document.getElementById("joint-holder-firstname_2").value.length > 2 &&
      document.getElementById("joint-holder-lastname_2").value.length > 2 &&
      document.getElementById("joint-holder-pannumber_2").value.length > 2 &&
      document.getElementById("joint-holder-mobile_2").value.length > 9 &&
      !this.state.joinFdFormDetect2
    ) {
      jointData2 = {
        holderFirstName: document.getElementById("joint-holder-firstname_2")
          .value,
        holderLastName: document.getElementById("joint-holder-lastname_2")
          .value,
        holderPanNumber: document.getElementById("joint-holder-pannumber_2")
          .value,
        holderMobile: document.getElementById("joint-holder-mobile_2").value
      };
      this.props.JointFdForm2(jointData2);
      this.setState({ joinFdFormDetect2: true });
    } else if (
      document.getElementById("joint-holder-firstname_2") &&
      document.getElementById("joint-holder-lastname_2") &&
      document.getElementById("joint-holder-pannumber_2") &&
      document.getElementById("joint-holder-mobile_2") &&
      (document.getElementById("joint-holder-firstname_2").value.length <= 2 ||
        document.getElementById("joint-holder-lastname_2").value.length <= 2 ||
        document.getElementById("joint-holder-pannumber_2").value.length <= 2 ||
        document.getElementById("joint-holder-mobile_2").value.length <= 9) &&
      this.state.joinFdFormDetect2
    ) {
      this.props.JointFdForm2(jointData2);
      this.setState({ joinFdFormDetect2: false });
    } else {
      this.props.enableJointFdForm(false);
    }
  };

  addMoreJoinee = value => {
    this.setState({ showAddMoreJoinee: value });
    if (!value) {
      this.setState({ joinFdFormDetect2: value });
      if (this.props.personalDetails.verified_otp_form1 === true) {
        this.props.enableJointFdForm(true);
      }
    } else {
      this.props.enableJointFdForm(false);
      this.props.clearVerifiedOTP();
    }
  };

  bankDetails = () => {
    if (this.props.match.params.Rj) {
      this.props.history.push("/bank-details/1", {
        user: this.props.location.state.user
      });
    } else {
      this.props.history.push("/bank-details");
    }
  };

  render() {
    return (
      <div className="vkgBtn">
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <Paper>
              <p className="addNomineeTxt">Add Nominee</p>
            </Paper>
          </Grid>
          <Grid item xs={3} className="reqOTP">
            <Paper>
              <ColorButton
                onClick={() => this.onclickBtn(1, true)}
                variant="contained"
                color="primary"
                className={this.state.addNomineeYes}
              >
                Yes
              </ColorButton>
            </Paper>
          </Grid>
          <Grid item xs={3} className="reqOTP">
            <Paper>
              <ColorButton
                onClick={() => this.onclickBtn(1, false)}
                variant="contained"
                color="primary"
                className={this.state.addNomineeNo}
              >
                No
              </ColorButton>
            </Paper>
          </Grid>
        </Grid>

        {this.state.showMe[1] ? (
          <div>
            <form className="textFieldWrapper" noValidate autoComplete="off">
              <TextField
                id="nominee-firstname"
                label="Nominee First Name"
                color="secondary"
                onChange={this.onChangeAddNomineeForm}
                value={this.props.personalDetails.addNomineeForm.firstName}
              />

              <TextField
                id="nominee-lastname"
                label="Nominee Last Name"
                color="secondary"
                onChange={this.onChangeAddNomineeForm}
                value={this.props.personalDetails.addNomineeForm.lastName}
              />

              <MenuItems options={currencies} />

              <Grid container spacing={1} className="paddingLeftNone">
                <Grid item xs={6}>
                  <Paper>
                    <p className="addNomineeTxt addNomineeTxt2 ">
                      Is the Nominee a minor?
                    </p>
                  </Paper>
                </Grid>
                <Grid item xs={3} className="reqOTP">
                  <Paper>
                    <ColorButton
                      onClick={() => this.onclickIsNomineeMinor(true)}
                      variant="contained"
                      color="primary"
                      className={this.state.isNomineeMinorYes}
                    >
                      Yes
                    </ColorButton>
                  </Paper>
                </Grid>
                <Grid item xs={3} className="reqOTP">
                  <Paper>
                    <ColorButton
                      onClick={() => this.onclickIsNomineeMinor(false)}
                      variant="contained"
                      color="primary"
                      className={this.state.isNomineeMinorNo}
                    >
                      No
                    </ColorButton>
                  </Paper>
                </Grid>
              </Grid>
              {this.state.isNomineeMinor === "yes" && (
                <div className="guardian">
                  <p className="nomineeTxt">
                    Since the nominee is a minor on this date, I/We appoint
                    below as guardian.
                  </p>
                  <TextField
                    id="nominee-guardian"
                    label="Name of Guardian"
                    color="secondary"
                    onChange={this.onChangeAddNomineeForm}
                    value={
                      this.props.personalDetails.addNomineeForm.guardianName
                    }
                  />
                </div>
              )}
            </form>
          </div>
        ) : null}

        <Grid container spacing={1}>
          <Grid item xs={6}>
            <Paper>
              <p className="addNomineeTxt">Is the FD for a minor?</p>
            </Paper>
          </Grid>
          <Grid item xs={3} className="reqOTP">
            <Paper>
              <ColorButton
                onClick={() => this.onclickBtn(2, true)}
                variant="contained"
                color="primary"
                className={this.state.fdMinorYes}
              >
                Yes
              </ColorButton>
            </Paper>
          </Grid>
          <Grid item xs={3} className="reqOTP">
            <Paper>
              <ColorButton
                onClick={() => this.onclickBtn(2, false)}
                variant="contained"
                color="primary"
                className={this.state.fdMinorNo}
              >
                No
              </ColorButton>
            </Paper>
          </Grid>
        </Grid>

        {this.state.showMe[2] ? (
          <div>
            <form className="textFieldWrapper" noValidate autoComplete="off">
              <AddDetailCalender
                selectedValue={this.props.personalDetails.fdMinorForm.minorDOB}
              />

              <TextField
                autoComplete="off"
                id="minor-firstName"
                label="First Name"
                color="secondary"
                onChange={this.onChangeForm}
                value={this.props.personalDetails.fdMinorForm.minorFirstName}
              />

              <TextField
                autoComplete="off"
                id="minor-middleName"
                label="Middle Name (Optional) "
                color="secondary"
                onChange={this.onChangeForm}
                value={this.props.personalDetails.fdMinorForm.minorMiddleName}
              />

              <TextField
                autoComplete="off"
                id="minor-lastName"
                label="Last Name"
                color="secondary"
                onChange={this.onChangeForm}
                value={this.props.personalDetails.fdMinorForm.minorLastName}
              />
            </form>
          </div>
        ) : null}

        <Grid container spacing={1}>
          <Grid item xs={6}>
            <Paper>
              <p className="addNomineeTxt">Is this a Joint FD?</p>
            </Paper>
          </Grid>
          <Grid item xs={3} className="reqOTP">
            <Paper>
              <ColorButton
                onClick={() => this.onclickBtn(3, true)}
                variant="contained"
                color="primary"
                className={this.state.jointFdYes}
              >
                Yes
              </ColorButton>
            </Paper>
          </Grid>
          <Grid item xs={3} className="reqOTP">
            <Paper>
              <ColorButton
                onClick={() => this.onclickBtn(3, false)}
                variant="contained"
                color="primary"
                className={this.state.jointFdNo}
              >
                No
              </ColorButton>
            </Paper>
          </Grid>
        </Grid>

        {this.state.showMe[3] ? (
          <div>
            <form className="textFieldWrapper" noValidate autoComplete="off">
              <TextField
                autoComplete="off"
                id="joint-holder-firstname_1"
                label="Joint Holder’s First Name"
                color="secondary"
                type="text"
                onChange={this.onChangeJointFdForm}
                value={this.props.personalDetails.jointFdForm.holderFirstName}
              />

              <TextField
                id="joint-holder-lastname_1"
                label="Last Name"
                color="secondary"
                type="text"
                onChange={this.onChangeJointFdForm}
                value={this.props.personalDetails.jointFdForm.holderLastName}
              />
              <TextField
                autoComplete="off"
                id="joint-holder-pannumber_1"
                label="Joint Holder’s PAN Number"
                color="secondary"
                onChange={this.onChangeJointFdForm}
                value={this.props.personalDetails.jointFdForm.holderPanNumber}
              />

              <TextField
                autoComplete="off"
                id="joint-holder-mobile_1"
                label="Enter Mobile Number"
                color="secondary"
                type="tel"
                onChange={this.onChangeJointFdForm}
                value={this.props.personalDetails.jointFdForm.holderMobile}
              />

              <Modal
                para={this.props.personalDetails.jointFdForm.holderMobile}
                mobileText="mobile"
                type="tel"
                heading="Verify Phone Number"
                verification="Enter 5 digit verification code"
                sendOTP={this.sendOTP}
                //para={document.getElementById("joint-holder-mobile_1").value}
                verifyOTP={this.handeVerifyOTP}
                form="form1"
                hanldeOTPText={this.hanldeOTPText}
                openModal={
                  this.props.personalDetails.verified_otp_form1 === true
                    ? false
                    : this.props.openModal
                }
                invalidOTP={this.props.invalid_otp}
                isValidPhone={this.state.joinFdFormDetect}
              />
            </form>
            <Grid container spacing={1}>
              <Grid item xs={9}>
                <Paper>
                  <p className="addNomineeTxt">Add More Joint holders</p>
                </Paper>
              </Grid>
              <Grid item xs={3} className="reqOTP">
                <Paper>
                  {!this.state.showAddMoreJoinee ? (
                    <ColorButton
                      onClick={() => this.addMoreJoinee(true)}
                      variant="contained"
                      color="primary"
                      className="buttonYesNo"
                    >
                      <AddCircleOutlineIcon className="plusIcon" />
                    </ColorButton>
                  ) : (
                    <ColorButton
                      onClick={() => this.addMoreJoinee(false)}
                      variant="contained"
                      color="primary"
                      className="buttonYesNo"
                    >
                      <IndeterminateCheckBoxOutlinedIcon className="plusIcon" />
                    </ColorButton>
                  )}
                </Paper>
              </Grid>
            </Grid>
            {this.state.showAddMoreJoinee && (
              <form className="textFieldWrapper" noValidate autoComplete="off">
                <TextField
                  autoComplete="off"
                  id="joint-holder-firstname_2"
                  label="Joint Holder’s First Name"
                  color="secondary"
                  type="text"
                  onChange={this.onChangeJointFdForm2}
                  value={
                    this.props.personalDetails.jointFdForm2.holderFirstName
                  }
                />

                <TextField
                  autoComplete="off"
                  id="joint-holder-lastname_2"
                  label="Last Name"
                  color="secondary"
                  type="text"
                  onChange={this.onChangeJointFdForm2}
                  value={this.props.personalDetails.jointFdForm2.holderLastName}
                />
                <TextField
                  autoComplete="off"
                  id="joint-holder-pannumber_2"
                  label="Joint Holder’s PAN Number"
                  color="secondary"
                  onChange={this.onChangeJointFdForm2}
                  value={
                    this.props.personalDetails.jointFdForm2.holderPanNumber
                  }
                />

                <TextField
                  autoComplete="off"
                  id="joint-holder-mobile_2"
                  label="Enter Mobile Number"
                  color="secondary"
                  type="tel"
                  onChange={this.onChangeJointFdForm2}
                  value={this.props.personalDetails.jointFdForm2.holderMobile}
                />

                <Modal
                  type="tel"
                  heading="Verify Phone Number"
                  verification="Enter 5 digit verification code"
                  sendOTP={this.sendOTP}
                  //para={document.getElementById("joint-holder-mobile_1").value}
                  verifyOTP={this.handeVerifyOTP}
                  form="form2"
                  hanldeOTPText={this.hanldeOTPText}
                  openModal={
                    this.props.personalDetails.verified_otp_form2 === true
                      ? false
                      : this.props.openModal
                  }
                  invalidOTP={this.props.invalid_otp}
                  isValidPhone={this.state.joinFdFormDetect2}
                />
              </form>
            )}
          </div>
        ) : null}

        <Button
          onClick={this.bankDetails}
          variant="contained"
          id="addDetailBtn"
          className="addDetailBtn"
        >
          Proceed
        </Button>
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    additionalDetails: store.additionalDetails,
    personalDetails: store.personalDetails,
    bankDetails: store.bankDetails
  };
}

const mapActionToProps = {
  enableAddNomineeForm,
  enableFdMinorForm,
  enableJointFdForm,
  sendEmailOTP,
  verifyEmailOTP,
  updateField,
  clearVerifiedOTP,
  addNomineeForm,
  fdMinorForm,
  JointFdForm,
  JointFdForm2,
  saveOptionalDetails,
  getNextAppNo
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(SimpleFade));
