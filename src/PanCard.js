import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Calender from "./Calender";
import moment from "moment";
import Instruction from "./Instruction";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import { connect } from "react-redux";

import {
  verifyPan,
  updateField,
  getDepositorsByPan,
  clearCallDepositorsPAN
} from "./actions/pancard_action";

import { withRouter } from "react-router";
import { clearUpdateLead } from "./actions/calculator_action";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: "65px"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  }
}));

const Pancard = props => {
  const classes = useStyles();
  const [loader, setLoader] = React.useState(false);
  console.log(props);
  const dateSelect = date => {
    let dob = moment(date).format("MM/DD/YYYY");
    props.updateField("dob", dob);
    props.verifyPan(props.pan_number, moment(date).format("DD/MM/YYYY"));
  };

  const proceed = () => {
    console.log("proceed---------");
    let data = {
      pan: props.pan_number,
      dob: moment(props.dob).format("DD/MM/YYYY"),
      type: "customer"
    };
    setLoader(true);
    props.getDepositorsByPan(data);
  };
  React.useEffect(
    () => {
      props.clearUpdateLead();
    } /* eslint-disable */,
    []
  );
  React.useEffect(() => {
    console.log("--------------------------", props);
    if (props.calledDepositorsPAN) {
      props.clearCallDepositorsPAN();
      props.history.push("/confirmation-personal-detail");
    }
  });

  return (
    <div className={classes.container}>
      <div className="panCardWrapper">
        <TextField
          autoComplete="off"
          id="standard-basic"
          label="PAN Number"
          inputProps={{
            maxLength: 10
          }}
          className={"vkg"}
          fullWidth
          onChange={e => {
            let newPan = e.target.value;
            if (newPan.length > 0 && newPan.length < 10) {
              props.updateField("is_valid_pan", false);
            } else {
              props.updateField("is_valid_pan", true);
              props.verifyPan(newPan, moment(props.dob).format("DD/MM/YYYY"));
            }
            props.updateField("message", "");
            props.updateField("pan_number", newPan);
          }}
          value={props.pan_number}
          style={{ marginTop: 30 }}
        />

        {props.message && <div className="verifiedMsg">{props.message}</div>}
        {!props.is_valid_pan && <div className="invalidMsg">Invalid PAN</div>}
      </div>
      <Calender onSelect={dateSelect} dob={props.dob} />
      <Instruction />
      <Button
        variant="contained"
        disabled={props.is_disable}
        color="primary"
        className="proceedButton"
        onClick={proceed}
      >
        Proceed
      </Button>

      {(props.is_loading || loader) && (
        <CircularProgress
          className={loader ? "progressBar proceed" : "progressBar"}
        />
      )}
    </div>
  );
};

const mapStateToProps = ({ pancard }) => ({
  ...pancard
});

const mapActionToProps = {
  verifyPan,
  updateField,
  clearUpdateLead,
  getDepositorsByPan,
  clearCallDepositorsPAN
};

export default connect(mapStateToProps, mapActionToProps)(withRouter(Pancard));
