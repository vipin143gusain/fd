import React, { useState } from "react";
export default function Test() {
  const [files, setFiles] = useState([]);

  const _onChange = event => {
    setFiles(event.target.files);
  };
  return (
    <div>
      <form>
        <input
          type="file"
          name="user[image]"
          multiple="true"
          value="upload"
          onChange={_onChange}
        />
      </form>

      {files &&
        [...files].map(file => <img alt="" src={URL.createObjectURL(file)} />)}
    </div>
  );
}
