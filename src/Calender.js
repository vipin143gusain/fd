import "date-fns";
import React from "react";
import Grid from "@material-ui/core/Grid";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import CalendarTodayIcon from "@material-ui/icons/CalendarToday";

export default function MaterialUIPickers(props) {
  // The first commit of Material-UI

  const handleDateChange = date => {
    console.log(date);
    props.onSelect(date);
  };
  console.log(props.dob);
  return (
    <div className="DateBith">
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container justify="space-around">
          <CalendarTodayIcon className="calenderIcon" />
          <DatePicker
            required
            className={"calenderBox"}
            variant="standard"
            format={"MM/dd/yyyy"}
            margin="normal"
            value={props.dob === "" ? null : props.dob}
            // emptyLabel="APGPG7899G"
            // placeholder="APGPG7899G"
            id="date-picker-inline"
            label="Date of Birth"
            onChange={handleDateChange}
            // hintText="Choose Date"
          />
        </Grid>
      </MuiPickersUtilsProvider>
    </div>
  );
}
