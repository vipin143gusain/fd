import React from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

// const currencies = [
//   {
//     value: "USD",
//     label: "Father"
//   },
//   {
//     value: "EUR",
//     label: "Mother"
//   },
//   {
//     value: "BTC",
//     label: "Spouse"
//   },
//   {
//     value: "JPY",
//     label: "Sister"
//   }
// ];

export default function MultilineTextFields(props) {
  const [currency, setCurrency] = React.useState("USD");
  const { options } = props;
  const handleChange = event => {
    setCurrency(event.target.value);
  };

  console.log("options", options);
  return (
    <div>
      <TextField
        className="menuItms"
        id="standard-select-currency"
        select
        label={props.label}
        value={currency}
        onChange={handleChange}
        helperText=""
      >
        {options.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
    </div>
  );
}
