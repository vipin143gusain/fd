import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import AppBar from "./AppBar"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: "65px"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
}));

export default function SimpleExpansionPanel() {
  const classes = useStyles();

  return (
    <div className="faqWrapper">
      <div className={classes.root}>
        <AppBar name="FAQs" />
        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>
              • Lorem ipsum dolor sit amet?
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse at dictum urna. Aliquam erat volutpat. Suspendisse
              magna nulla, porta in nulla nec, suscipit ornare risus. Vestibulum
              id mi urna. Aenean vel vehicula metus, eu tincidunt tortor.
              Curabitur faucibus molestie augue et fringilla. Curabitur
              efficitur dictum dolor, et vehicula nisl interdum non.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>
              • Lorem ipsum dolor sit amet?
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse at dictum urna. Aliquam erat volutpat. Suspendisse
              magna nulla, porta in nulla nec, suscipit ornare risus. Vestibulum
              id mi urna. Aenean vel vehicula metus, eu tincidunt tortor.
              Curabitur faucibus molestie augue et fringilla. Curabitur
              efficitur dictum dolor, et vehicula nisl interdum non.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>
              • Lorem ipsum dolor sit amet?
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse at dictum urna. Aliquam erat volutpat. Suspendisse
              magna nulla, porta in nulla nec, suscipit ornare risus. Vestibulum
              id mi urna. Aenean vel vehicula metus, eu tincidunt tortor.
              Curabitur faucibus molestie augue et fringilla. Curabitur
              efficitur dictum dolor, et vehicula nisl interdum non.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>
              • Lorem ipsum dolor sit amet?
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse at dictum urna. Aliquam erat volutpat. Suspendisse
              magna nulla, porta in nulla nec, suscipit ornare risus. Vestibulum
              id mi urna. Aenean vel vehicula metus, eu tincidunt tortor.
              Curabitur faucibus molestie augue et fringilla. Curabitur
              efficitur dictum dolor, et vehicula nisl interdum non.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>
              • Lorem ipsum dolor sit amet?
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse at dictum urna. Aliquam erat volutpat. Suspendisse
              magna nulla, porta in nulla nec, suscipit ornare risus. Vestibulum
              id mi urna. Aenean vel vehicula metus, eu tincidunt tortor.
              Curabitur faucibus molestie augue et fringilla. Curabitur
              efficitur dictum dolor, et vehicula nisl interdum non.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>
              • Lorem ipsum dolor sit amet?
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse at dictum urna. Aliquam erat volutpat. Suspendisse
              magna nulla, porta in nulla nec, suscipit ornare risus. Vestibulum
              id mi urna. Aenean vel vehicula metus, eu tincidunt tortor.
              Curabitur faucibus molestie augue et fringilla. Curabitur
              efficitur dictum dolor, et vehicula nisl interdum non.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>
              Expansion Panel 2
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
              eget.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3a-content"
            id="panel3a-header"
          >
            <Typography className={classes.heading}>
              Disabled Expansion Panel
            </Typography>
          </ExpansionPanelSummary>
        </ExpansionPanel>
      </div>
    </div>
  );
}
