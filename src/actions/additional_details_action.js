import {
  ADD_NOMINEE_FORM_ENABLE,
  FD_MINOR_FORM_ENABLE,
  JOINT_FD_FORM_ENABLE
} from "../constants";

export const enableAddNomineeForm = value => dispatch => {
  dispatch({
    type: ADD_NOMINEE_FORM_ENABLE,
    payload: value
  });
};

export const enableFdMinorForm = value => dispatch => {
  dispatch({
    type: FD_MINOR_FORM_ENABLE,
    payload: value
  });
};

export const enableJointFdForm = value => dispatch => {
  dispatch({
    type: JOINT_FD_FORM_ENABLE,
    payload: value
  });
};
