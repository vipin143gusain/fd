import axios from "axios";
import { API_ROOT } from "../config";

export const uploadDocumentCall = uploadFormData => async dispatch => {
  /**
   * params sample
   * {
          "type": "store",
          "application_no": "CU000898",
          "docNames": {
              "panCard": "pan.jpg",
              "proofOfAddress": "POA.pdf"
          },
          "token": "ccsdf3asf2434dd"
      }
   */
  // console.log("Action Param", params);
  // console.log("Action ZipFile", file);
  // var uploadFormData = new FormData();
  // uploadFormData.set("param", params);
  // uploadFormData.append("zipFile", file);

  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/uploadDocuments`,
    uploadFormData,
    { headers: { "Content-Type": "multipart/form-data" } }
  );
  console.log("Response", request);
  let data = request.data;
  dispatch({
    type: "RESPONSE_DOC_UPLOAD",
    payload: { fieldName: "message", fieldValue: data.message }
  });
  if (data && data.code === "DS") {
    return true;
  }
  return false;
};

export const summaryDetails = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/getSummaryDetails`,
    value
  );
  let data = request.data;
  if (data.code === "DF") {
    dispatch({
      type: "SUMMARY_DETAILS",
      payload: { summaryDetails: data.data }
    });
  }
};

export const getDocumentList = () => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/documentList`
  );
  let data = request.data;
  if (data.code === "DF") {
    dispatch({
      type: "DOCUMENT_LIST",
      payload: data
    });
  }
};

export const storeDocumentList = data => async dispatch => {
  dispatch({
    type: "STORE_DOCUMENT_LIST",
    payload: data
  });
};

export const makePaymentAPI = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/payment`,
    value
  );
  let data = request.data;
  if (data.code === "URL") {
    dispatch({
      type: "MAKE_PAYMENT",
      payload: data
    });
  }
};

export const createReceiptPDF = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/createReceiptPdf`,
    value
  );
  let data = request.data;
  if (data.code === "SCS") {
    // const url = window.URL.createObjectURL(new Blob([data]));
    // const link = document.createElement("a");
    // link.href = url;
    // link.setAttribute("download", data.data);
    // document.body.appendChild(link);
    // link.click();

    window.open(data.data, "_blank");

    dispatch({
      type: "CREATE_RECEIPT_PDF",
      payload: data
    });
  }
};
