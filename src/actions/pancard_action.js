import axios from "axios";
import { API_ROOT } from "../config";

export const verifyPan = (panNumber, dob) => async dispatch => {
  dispatch({
    type: "UPDATE_FIELD",
    payload: {
      fieldName: "message",
      fieldValue: ""
    }
  });
  if (dob.length === 10 && panNumber.length === 10) {
    console.log("panNumber : ", panNumber);
    dispatch({ type: "SET_LOADING" });
    const request = await axios.post(
      `${API_ROOT}/fd_management/verify/getPanDetails`,
      {
        pan: panNumber,
        dob: dob
      }
    );
    let data = request.data;

    if (data.message !== "Invalid PAN" && Object.entries(data).length !== 0) {
      dispatch({
        type: "UPDATE_FIELD",
        payload: { fieldName: "message", fieldValue: data.message }
      });
      dispatch({
        type: "UPDATE_FIELD",
        payload: { fieldName: "is_disable", fieldValue: false }
      });

      dispatch({
        type: "UPDATE_FIELD",
        payload: { fieldName: "is_loading", fieldValue: false }
      });

      var splitDate = dob.split("/");
      if (splitDate.count === 0) {
        return null;
      }
      var year = splitDate[0];
      var month = splitDate[1];
      var day = splitDate[2];
      // dispatch({
      //   type: "UPDATE_FIELD",
      //   payload: { fieldName: "is_disable", fieldValue: false }
      // });
      dispatch({
        type: "UPDATE_DATA",
        payload: {
          first_name: data.data.fname,
          last_name: data.data.lname,
          dob: day + "/" + month + "/" + year
        }
      });
    } else {
      dispatch({
        type: "UPDATE_FIELD",
        payload: { fieldName: "is_valid_pan", fieldValue: false }
      });
      dispatch({
        type: "UPDATE_FIELD",
        payload: { fieldName: "is_disable", fieldValue: true }
      });

      dispatch({
        type: "UPDATE_FIELD",
        payload: { fieldName: "is_loading", fieldValue: false }
      });
    }
  }
};

export const updateField = (fieldName, fieldValue) => dispatch => {
  dispatch({
    type: "UPDATE_FIELD",
    payload: {
      fieldValue,
      fieldName
    }
  });
};

export const getDepositorsByPan = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/getDepositorsByPan`,
    value
  );
  let data = request.data;
  if (data.code === "PF" || data.code === "PNF") {
    dispatch({
      type: "DEPOSITORS_PAN",
      payload: {
        panName: data.panName,
        depositorsData: data.data,
        calledDepositorsPAN: true
      }
    });
  }
};

export const clearCallDepositorsPAN = () => dispatch => {
  dispatch({
    type: "DEPOSITORS_PAN",
    payload: {
      calledDepositorsPAN: false
    }
  });
};

export const getStateList = () => async dispatch => {
  const request = await axios.get(
    `${API_ROOT}/fd_management/applicants/getStatesList`
  );
  let data = request.data;

  if (data.status === "success") {
    console.log("State", data.status);
    dispatch({
      type: "STATE_LIST",
      payload: data.response
    });
  }
};

export const getCityList = statecode => async dispatch => {
  const request = await axios.get(
    `${API_ROOT}/fd_management/applicants/getCityListByStateCode?statecode=${statecode}`
  );
  let data = request.data;
  if (data.status === "success") {
    dispatch({
      type: "CITY_LIST",
      payload: data.response
    });
  }
};

export const setStateName = value => dispatch => {
  dispatch({
    type: "SET_STATE_NAME",
    payload: value
  });
};

export const setCityName = value => dispatch => {
  dispatch({
    type: "SET_CITY_NAME",
    payload: value
  });
};
