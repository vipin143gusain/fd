import axios from "axios";

export const investAmountUpdateLead = value => async dispatch => {
  const request = await axios.post(
    "https://fgfinancialservices.in/fixedDeposit/onboard/updateLead",
    value
  );
  let data = request.data;
  if (data.code === "DS") {
    dispatch({
      type: "UPDATE_LEAD",
      payload: { updatedleadaction: true }
    });
  }
};

export const clearUpdatedLead = () => dispatch => {
  dispatch({
    type: "UPDATE_LEAD",
    payload: { updatedleadaction: false }
  });
};
