import axios from "axios";
import { API_ROOT } from "../config";

export const sendEmailOTP = (type, value) => async dispatch => {
  const request = await axios.post(`${API_ROOT}/fd_management/otp/sendOtp`, {
    contact: value,
    type,
    template: "verify"
  });
  let data = request.data;
  if (data.code === "OTPS") {
    dispatch({
      type: "SEND_OTP",
      payload: { fieldName: "is_otp_send", fieldValue: true }
    });
    dispatch({
      type: "UPDATE_FIELD",
      payload: { fieldName: "is_otp_send", fieldValue: false }
    });
  }
};

export const verifyEmailOTP = (value, otp, form) => async dispatch => {
  const request = await axios.post(`${API_ROOT}/fd_management/otp/verifyOtp`, {
    contact: value,
    otp: otp
  });
  let data = request.data;
  if (data.code === "OTPM") {
    if (form === "form1") {
      dispatch({
        type: "UPDATE_FIELD_FORM1",
        payload: { fieldName: "is_otp_verified", fieldValue: true }
      });
    } else if (form === "form2") {
      dispatch({
        type: "UPDATE_FIELD_FORM2",
        payload: {
          fieldName: "is_otp_verified",
          fieldValue: true
        }
      });
    } else {
      dispatch({
        type: "UPDATE_FIELD",
        payload: {
          fieldName: "is_otp_verified",
          fieldValue: true
        }
      });
    }
  } else {
    dispatch({
      type: "UPDATE_FIELD",
      payload: { fieldName: "invalid_otp", fieldValue: data.message }
    });
  }
};

export const clearVerifiedOTP = () => dispatch => {
  dispatch({
    type: "UPDATE_FIELD_CLEAR",
    payload: { fieldName: "is_otp_send", fieldValue: false }
  });
};

export const addNomineeForm = data => dispatch => {
  dispatch({
    type: "ADD_NOMINEE_FORM",
    payload: data
  });
};

export const fdMinorForm = data => dispatch => {
  dispatch({
    type: "FD_MINOR_FORM",
    payload: data
  });
};

export const JointFdForm = data => dispatch => {
  dispatch({
    type: "JOINT_FD_FORM",
    payload: data
  });
};

export const JointFdForm2 = data => dispatch => {
  dispatch({
    type: "JOINT_FD_FORM2",
    payload: data
  });
};

export const saveOptionalDetails = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/saveOptionalDetails`,
    value
  );
  let data = request.data;
  if (data.code === "URL") {
    dispatch({
      type: "SAVE_OPTIONAL",
      payload: data
    });
  }
};
