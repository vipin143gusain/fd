import axios from "axios";

export const fetchDepositDetails = mobNo => async dispatch => {
  const request = await axios.post(
    `http://182.74.108.188:8080/fd_management/loginController/getdepositdetails/?mobile_number=${mobNo}`
  );
  let data = request.data;
  if (data.status === "success") {
    dispatch({
      type: "DEPOSIT_DETAILS",
      payload: {
        data: data.data,
        totalAmount: data.totalAmount,
        totalInterest: data.totalInterest
      }
    });
  }
};

export const fetchCustomerDetails = mobNo => async dispatch => {
  const request = await axios.post(
    `http://182.74.108.188:8080/fd_management/onboard/getProfilesByMobile`,
    {
      mobile: mobNo,
      minor: "N"
    }
  );
  let data = request.data;
  if (data.code === "DF") {
    console.log("actoion -----------", data);
    dispatch({
      type: "EXISTING_CUSTOMER",
      payload: {
        customers: data.data
      }
    });
  }
};
