import {
  UPDATE_YEAR,
  UPDATE_PAUOUT_OPTION,
  UPDATE_AMOUNT,
  UPDATE_INTEREST,
  ENABLE_EDIT,
  UPDATE_ROI
} from "../constants";
import { API_ROOT } from "../config";
import axios from "axios";

export const updateYear = year => dispatch => {
  dispatch({
    type: UPDATE_YEAR,
    payload: year
  });
};

export const updatePayoutOption = option => dispatch => {
  dispatch({
    type: UPDATE_PAUOUT_OPTION,
    payload: option
  });
};

export const updateAmount = amount => dispatch => {
  dispatch({
    type: UPDATE_AMOUNT,
    payload: amount
  });
};

export const updateInterestAmount = amount => dispatch => {
  dispatch({
    type: UPDATE_INTEREST,
    payload: amount
  });
};

export const enableEdit = () => dispatch => {
  dispatch({
    type: ENABLE_EDIT,
    payload: true
  });
};

export const saveLead = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/saveLead`,
    {
      name: "VIPIN KUMAR GUSAIN",
      mobile: "9953756175",
      email: "vipin143gusain@gmail.com",
      remark: "",
      source: "undefined"
    }
  );
  let data = request.data;
  if (data.code === "DS") {
    dispatch({
      type: "SAVED_LEAD",
      payload: { leadID: data.data.Id }
    });
  } else {
    dispatch({
      type: "SAVED_LEAD",
      payload: { leadID: false }
    });
  }
};

export const updateLead = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/updateLead`,
    value
  );
  let data = request.data;
  if (data.code === "DS") {
    dispatch({
      type: "UPDATE_LEAD",
      payload: { updatedLead: true }
    });
  } else {
    dispatch({
      type: "UPDATE_LEAD",
      payload: { updatedLead: false }
    });
  }
};

export const updateFDAccount = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/updateFdDetails`,
    value
  );
  let data = request.data;
  if (data.code === "DS") {
    dispatch({
      type: "UPDATE_FD_ACCOUNT",
      payload: { updateFdAccount: true }
    });
  } else {
    dispatch({
      type: "UPDATE_FD_ACCOUNT",
      payload: { updateFdAccount: false }
    });
  }
};

export const updateROI = roi => dispatch => {
  dispatch({
    type: UPDATE_ROI,
    payload: roi
  });
};

export const clearUpdateLead = () => dispatch => {
  dispatch({
    type: "UPDATE_LEAD",
    payload: false
  });
};

export const clearUpdateFDAccount = () => dispatch => {
  dispatch({
    type: "UPDATE_FD_ACCOUNT",
    payload: { updateFdAccount: false }
  });
};
