import axios from "axios";
import { API_ROOT } from "../config";

export const verifyAccountDetails = (
  accountNumber,
  ifsc,
  applicantFullName
) => async dispatch => {
  console.log("accountNumber : ", accountNumber);
  console.log("ifsc : ", ifsc);
  // dispatch({
  //     type: "UPDATE_FIELD",
  //     payload: {
  //         fieldName: "message",
  //         fieldValue: ""
  //     }
  // });
  // if (dob.length === 10) {
  // console.log("panNumber : ", panNumber);
  // dispatch({ type: "SET_LOADING" });
  const request = await axios.post(
    `${API_ROOT}/fd_management/verify/verifyBankAccount`,
    {
      accountNumber,
      ifsc,
      applicantFullName
    }
  );
  let data = request.data;
  console.log(data);
  if (
    data.code === "IA" ||
    data.code === "LE" ||
    data.code === "IC" ||
    data.code === "NM" ||
    data.code === "RY" ||
    data.code === "INV_IFSC"
  ) {
    dispatch({
      type: "UPDATE_FIELD",
      payload: { fieldName: "invalid_account", fieldValue: true }
    });

    dispatch({
      type: "VERIFY_BANK_ACCOUNT",
      payload: {
        verifyBankData: false,
        verifyBankAccName: false,
        verifyBankMessage: false,
        verifyBankStatus:
          data.code === "IA"
            ? "Invalid Account"
            : data.code === "LE"
            ? "Limit Exceed"
            : data.code === "IC"
            ? "Insufficient Credits"
            : data.code === "NM"
            ? "Name Mismatch"
            : data.code === "RY"
            ? "Retry"
            : data.code === "INV_IFSC"
            ? "Invalid IFSC"
            : ""
      }
    });
  }

  if (data.code === "AV") {
    dispatch({
      type: "UPDATE_FIELD",
      payload: { fieldName: "is_acc_verified", fieldValue: true }
    });

    dispatch({
      type: "VERIFY_BANK_ACCOUNT",
      payload: {
        verifyBankData: data.data,
        verifyBankAccName: data.accName,
        verifyBankMessage: data.message,
        verifyBankStatus: "Account Verified"
      }
    });
  }

  // dispatch({
  //     type: "UPDATE_FIELD",
  //     payload: { fieldName: "message", fieldValue: data.message }
  // });
  // if (data.message !== "Invalid PAN") {
  //     dispatch({
  //         type: "UPDATE_DATA",
  //         payload: {
  //             first_name: data.data.fname,
  //             last_name: data.data.lname,
  //             dob
  //         }
  //     });
  // } else {
  //     dispatch({
  //         type: "UPDATE_FIELD",
  //         payload: { fieldName: "is_loading", fieldValue: false }
  //     });
  // }
  // }
};

export const getNextAppNo = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/getNextAppNo`,
    value
  );
  let data = request.data;
  if (data.code === "SCS") {
    dispatch({
      type: "NEXT_APP_NO",
      payload: { ccNumber: data.data }
    });
  }
};

export const saveCustomer = value => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/saveCustomer`,
    value
  );
  let data = request.data;
  if (data.code === "CS") {
    dispatch({
      type: "SAVE_CUSTOMER",
      payload: { saveCustomer: true }
    });
  }
};

export const updateBankDetails = (
  accNo,
  ifsc,
  appNo,
  accName,
  nameMatched
) => async dispatch => {
  const request = await axios.post(
    `${API_ROOT}/fd_management/onboard/updateBankDetailsByAppNo`,
    {
      accNo,
      ifsc,
      appNo,
      accName,
      nameMatched
    }
  );
  let data = request.data;
  console.log("update bank details", data);
  if (data.code === "DS") {
    dispatch({
      type: "UPDATE_FIELD",
      payload: { fieldName: "success", fieldValue: true }
    });
  }
};
