import React, { useState } from "react";
import "./App.css";
import AppBar from "./AppBar";
import BankDetailCard from "./BankDetailCard";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { updateField } from "./actions/pancard_action";
import {
  verifyAccountDetails,
  getNextAppNo,
  saveCustomer,
  updateBankDetails
} from "./actions/bank_details_action";
import { saveOptionalDetails } from "./actions/personal_details_action";
import VerifiedIcon from "./VerifiedIcon";

import {
  createMuiTheme,
  responsiveFontSizes,
  ThemeProvider
} from "@material-ui/core/styles";

const App = props => {
  const [verifyAccountAPI, setVerifyAccountAPI] = useState(false);
  const [verifyAccountStatus, setVerifyAccountStatus] = useState(false);
  const [saveCustomerAPI, setSaveCustomerAPI] = useState(false);
  const [saveOptionalAPI, setSaveOptionalAPI] = useState(false);
  const [hideVerify, setVerify] = useState(true);

  console.log("props", props);

  const updateData = (name, value) => {
    props.updateField(name, value);
  };

  /* eslint-disable */

  React.useEffect(() => {
    if (props.bankDetails.account_number && props.bankDetails.ifsc_code) {
      console.log("data exists");
      if (
        props.bankDetails.account_number ===
        props.bankDetails.verify_account_number
      ) {
        console.log("matched");
        setVerify(false);
      } else {
        setVerify(true);
      }
    } else {
      setVerify(true);
    }
  });

  React.useEffect(
    () => {
      console.log("did mnount------------------------");
      if (props.bankDetails.verifyBankAccName && !verifyAccountAPI) {
        setVerifyAccountAPI(true);

        updateBankDetails();
        // let data = {
        //   type: "customer"
        // };
        // props.getNextAppNo(data);
        //props.history.push("/deposit-summary");
      }
    } /* eslint-disable */,
    [props, verifyAccountAPI]
  );

  React.useEffect(() => {
    if (props.bankDetails.success && verifyAccountAPI) {
      props.history.push("/deposit-summary", {
        user: props.location.state.user
      });
    }
  }, [props, verifyAccountAPI]);

  React.useEffect(() => {
    if (!props.bankDetails.verifyBankAccName && !verifyAccountAPI) {
      setVerifyAccountStatus(props.bankDetails.verifyBankStatus);
    }
  }, [props, verifyAccountAPI]);

  React.useEffect(() => {
    if (props.bankDetails.ccNumber && !saveCustomerAPI) {
      console.log("MundaVaadi", props);
      let stateN = "";
      let cityN = "";
      let minorDB = "";
      let isMinor = false;
      if (
        props.personalDetails &&
        props.personalDetails.fdMinorForm &&
        Object.keys(props.personalDetails.fdMinorForm).length > 0 &&
        props.personalDetails.fdMinorForm.minorDOB
      ) {
        minorDB = props.personalDetails.fdMinorForm.minorDOB;
        let splitMinorDB = minorDB.split("-");
        minorDB =
          splitMinorDB[0] + "/" + splitMinorDB[1] + "/" + splitMinorDB[2];
        isMinor = true;
      }
      if (props.pancard && props.pancard.stateList) {
        console.log(
          "State Array",
          props.pancard.stateList,
          typeof props.pancard.stateList
        );
        console.log(
          "City Array",
          props.pancard.cityList,
          typeof props.pancard.cityList
        );
        let stateArray = props.pancard.stateList.find(
          item => item.stateCode === parseInt(props.pancard.getStateName)
        );
        console.log("S", stateArray);
        stateN = stateArray.stateName;
        let cityArray = props.pancard.cityList.find(
          item => item.cityCode === parseInt(props.pancard.getCityName)
        );
        console.log("C", cityArray);
        cityN = cityArray.cityName;
      }
      console.log("navchandan", props);
      var data;
      if (props.match.params.Rj) {
        data = {
          pan: props.location.state.user.PANNo,
          dob: props.location.state.user.DOB,
          full_name: props.location.state.user.DepositorName,
          city: "MUMBAI",
          journeyType: "customer",
          bank_acc_no: props.bankDetails.account_number,
          ifsc: props.bankDetails.ifsc_code,
          fdDetails: [
            {
              application_no: props.bankDetails.ccNumber,
              fd_amount: props.calculator.amount,
              maturity_amount:
                Math.abs(props.calculator.amount) +
                Math.abs(props.calculator.total_interest),
              quarter_payout: 0,
              roi: props.calculator.roi,
              special: true,
              schemeType: props.calculator.payout_option === "MP" ? "CS" : "PS",
              tenure: props.calculator.year,
              maturity_date: "11/12/2021",
              remark: null,
              source: "undefined"
            }
          ]
        };
      } else {
        data = {
          pan: props.bankDetails.pan_number,
          type: "I",
          dob: props.pancard.dob,
          mobile: "9953756175",
          email: props.bankDetails.email,
          gender: "M",
          salutation: 1,
          f_name: props.bankDetails.first_name,
          m_name: "",
          l_name: props.bankDetails.last_name,
          full_name: props.bankDetails.first_name + props.bankDetails.last_name,
          d_fname: "rahul ",
          d_mname: "minor",
          d_lname: "testt",
          d_full_name: " rahul  minor testt",
          minorDob: minorDB,
          differentName: false,
          minor: isMinor,
          special_cat: "F",
          special_cat_number: "9953756175",
          address1: props.pancard.addr_line1,
          address2: props.pancard.addr_line2,
          state: stateN,
          state_code: props.pancard.getStateName,
          city: cityN,
          city_code: props.pancard.getCityName,
          pincode: props.bankDetails.pincode,
          doc_names: "",
          city: cityN,
          journeyType: "customer",
          bank_acc_no: props.bankDetails.account_number,
          ifsc: props.bankDetails.ifsc_code,
          ecsBranchCode: 0,
          existing: false,
          accName: props.bankDetails.verifyBankAccName,
          fdDetails: [
            {
              application_no: props.bankDetails.ccNumber,
              fd_amount: props.calculator.amount,
              maturity_amount:
                Math.abs(props.calculator.amount) +
                Math.abs(props.calculator.total_interest),
              quarter_payout: 0,
              roi: props.calculator.roi,
              special: true,
              schemeType: props.calculator.payout_option === "MP" ? "CS" : "PS",
              tenure: props.calculator.year,
              maturity_date: "11/12/2021",
              remark: null,
              source: "undefined"
            }
          ]
        };
      }
      // setTimeout(function() {
      //   props.history.push("/deposit-summary");
      // }, 3000);
      setSaveCustomerAPI(true);
      props.saveCustomer(data);
    }
  }, [props, saveCustomerAPI]);

  React.useEffect(() => {
    if (props.bankDetails.saveCustomer && !saveOptionalAPI) {
      let checkisNominee = false;
      let nominee = {};
      let guardian_id = {};
      let applicantMinor = false;
      let minorApplicant = {};
      let jointFdForm = false;
      let additionalApplicants = [];

      if (
        props.personalDetails.addNomineeForm &&
        Object.keys(props.personalDetails.addNomineeForm).length > 0
      ) {
        checkisNominee = true;
        if (
          props.personalDetails.addNomineeForm &&
          props.personalDetails.addNomineeForm.guardianName
        ) {
          guardian_id = {
            name: props.personalDetails.addNomineeForm.guardianName
          };
        }
        nominee = {
          name: props.personalDetails.addNomineeForm.firstName,
          relation: "son",
          minor: props.personalDetails.addNomineeForm.nomineeMinor
            ? true
            : false,
          guardian_id: guardian_id
        };
      }

      if (
        props.personalDetails.fdMinorForm &&
        Object.keys(props.personalDetails.fdMinorForm).length > 0
      ) {
        applicantMinor = true;

        let minorDOB = false;
        if (props.personalDetails.fdMinorForm.minorDOB) {
          let splitDB = props.personalDetails.fdMinorForm.minorDOB.split("-");
          minorDOB = splitDB[1] + "/" + splitDB[2] + "/" + splitDB[0];
        }
        minorApplicant = {
          d_fname: props.personalDetails.fdMinorForm.minorFirstName,
          d_mname: props.personalDetails.fdMinorForm.minorLastName,
          d_lname: props.personalDetails.fdMinorForm.minorMiddleName,
          d_dob: minorDOB
        };
      }

      if (
        props.personalDetails.jointFdForm &&
        Object.keys(props.personalDetails.jointFdForm).length > 0
      ) {
        jointFdForm = true;
        if (props.personalDetails.jointFdForm.holderFirstName) {
          additionalApplicants = [
            {
              f_name: props.personalDetails.jointFdForm.holderFirstName,
              l_name: props.personalDetails.jointFdForm.holderLastName,
              pan: props.personalDetails.jointFdForm.holderPanNumber
            }
          ];
        }
      }

      let data = {
        applicationNo: props.bankDetails.ccNumber,
        checkisNominee: checkisNominee,
        nominee: nominee,
        applicantMinor: applicantMinor,
        minorApplicant: minorApplicant,
        jointFd: jointFdForm,
        additionalApplicants: additionalApplicants
      };

      console.log("needed props", props);
      props.saveOptionalDetails(data);
      setSaveOptionalAPI(true);
      // props.history.push("/deposit-summary");
    }
  }, [props, saveOptionalAPI]);

  React.useEffect(() => {
    console.log("after optional props", props);
  });

  const verifyAccount = () => {
    console.log("onClick");
    setVerifyAccountAPI(false);
    props.verifyAccountDetails(
      props.bankDetails.account_number,
      props.bankDetails.ifsc_code,
      props.bankDetails.first_name + props.bankDetails.last_name
    );
  };

  const updateBankDetails = () => {
    props.updateBankDetails(
      props.bankDetails.account_number,
      props.bankDetails.ifsc_code,
      props.bankDetails.ccNumber,
      props.bankDetails.verifyBankAccName,
      props.bankDetails.verifyBankData.namePercent
    );
  };

  let theme = createMuiTheme();
  theme = responsiveFontSizes(theme);
  let ui = !props.is_acc_verified ? (
    <div className="App">
      <AppBar name="Enter Bank Details" />
      <BankDetailCard
        updateData={updateData}
        invalid_account={props.invalid_account}
        invalid_ifsc={props.invalid_ifsc}
        invalid_verify_account={props.bankDetails.invalid_verify_account}
      />
      {console.log("adfasfsdggts", props)}
      {verifyAccountStatus && !props.bankDetails.verifyBankAccName && (
        <div> {verifyAccountStatus}</div>
      )}
      <ThemeProvider theme={theme}>
        <Typography className="instruction" variant="h6">
          *To verify bank details we will credit Rs. 1 to your account
        </Typography>
      </ThemeProvider>
      {/* <div className="uploadWrapper">
        <ThemeProvider theme={theme}>
          <Typography className="instruction2 instruction" variant="h6">
            Upload copy of Cancelled Cheque
          </Typography>

            * Mandatory Field
          </Typography>

          <Grid container spacing={1}>
            <Grid item xs={12} className="reqOTP">
              <div>
                <form>
                  <label className="custom-file-upload">
                    <input
                      type="file"
                      name="user[image]"
                      multiple="true"
                      onChange={_onChange}
                    />
                    Upload
                  </label>
                </form>
              </div>
            </Grid>
            {/* <Grid item xs={2}>
              <p className="addNomineeTxt">Or</p>
            </Grid> 
            {/* <Grid item xs={5} className="reqOTP">
              <div>
                <form>
                  <label class="custom-file-upload">
                    <input
                      type="file"
                      name="user[image]"
                      multiple="true"
                      onChange={_onChange}
                    />
                    Capture
                  </label>
                </form>
              </div>
            </Grid> 
          </Grid>

          <div className="uploadDocs">
            <uploadDocsImg />
            {files &&
              [...files].map(file => (
                <img
                  alt=""
                  className="uploadImagPreview"
                  src={URL.createObjectURL(file)}
                />
              ))}
          </div>
        </ThemeProvider>
      </div> */}

      <Button
        variant="contained"
        color="primary"
        className="proceedButtonDisable"
        onClick={verifyAccount}
        disabled={hideVerify}
      >
        VERIFY DETAILS
      </Button>
    </div>
  ) : (
    <VerifiedIcon changeTxt="Account Verified!" />
  );
  return ui;
};

// const mapStateToProps = ({ bankDetails }) => ({
//   ...bankDetails
// });

function mapStateToProps(store) {
  console.log("Redux Store", store);
  return {
    bankDetails: store.bankDetails,
    pancard: store.pancard,
    calculator: store.calculator,
    additionalDetails: store.additionalDetails,
    personalDetails: store.personalDetails,
    investAmountDetails: store.investAmountDetails,
    uploadDocuments: store.uploadDocuments
  };
}

const mapActionToProps = {
  updateField,
  verifyAccountDetails,
  getNextAppNo,
  saveCustomer,
  saveOptionalDetails,
  updateBankDetails
};

export default connect(mapStateToProps, mapActionToProps)(withRouter(App));
