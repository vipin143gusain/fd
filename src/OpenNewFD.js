import React, { Component } from "react";
import Tab from "./Tab";
import AppBar from "./AppBar";
import BG from "./images/congratulationsBg.svg";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

export default class OpenNewFD extends Component {
  render() {
    return (
      <div>
        <AppBar name="Open New FD" />
        <div className="newFD">
          <div className="heroBanner">
            <img src={BG} alt="" />
          </div>
          <Button variant="contained" color="primary">
            <Link className="linkColorChange" to="/invest-amount">
              Invest Now
            </Link>
          </Button>

          <Button variant="contained" color="primary">
            Existing User? Log In
          </Button>

          <Tab />
        </div>
      </div>
    );
  }
}
