import React, { useState } from "react";
import AppBar from "./AppBar";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import { Checkbox } from "@material-ui/core";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import {
  summaryDetails,
  makePaymentAPI
} from "./actions/upload_documents_action";
import { clearUpdateFDAccount } from "./actions/calculator_action";

const editFunc = props => {
  console.log("editFunc------", props.bankDetails.ccNumber);
  //document.getElementById("myP").contentEditable = true;
  var Cn = props.bankDetails.ccNumber
    ? props.bankDetails.ccNumber
    : props.match.params.Cn;
  console.log(props.bankDetails.ccNumber, props.match.params.Cn);
  props.history.push("/invest-amount/" + Cn);
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

const DepositSummary = props => {
  const classes = useStyles();
  // const [files, setFiles] = useState(0);
  const [rcvMode, setRCVMode] = useState("D");
  const [enableProceed, setEnableProceed] = useState(true);

  const [agreeOne, setAgreeOne] = useState(false);
  const [agreeTwo, setAgreeTwo] = useState(false);

  // let nomineeName = "----";

  console.log("P", props);

  const aggreeOne = () => {
    var checkBox = document.getElementById("aggree1");
    console.log("chhhh", checkBox.checked);
    if (checkBox.checked === true) {
      setAgreeOne(true);
    } else {
      setAgreeOne(false);
    }
  };

  const aggreeTwo = () => {
    var checkBox2 = document.getElementById("aggree2");
    console.log("chhhh 2", checkBox2.checked);
    if (checkBox2.checked === true) {
      setAgreeTwo(true);
    } else {
      setAgreeTwo(false);
    }
  };

  const proceedPayment = () => {
    let data = {
      type: "customer",
      application_no: props.summary.application_no,
      pay_mode: "O",
      fd_rcv_mode: rcvMode,
      rdrUrl:
        "http://192.168.1.5:3000/upload-documents/" +
        rcvMode +
        "/" +
        props.bankDetails.ccNumber
    };
    console.log("props payment", props);
    props.makePaymentAPI(data);
  };
  /* eslint-disable */

  React.useEffect(() => {
    let data = {
      type: "customer",
      application_no: props.bankDetails.ccNumber
        ? props.bankDetails.ccNumber
        : props.match.params.Cn
    };
    // props.setFiles(1);
    props.summaryDetails(data);
  }, []);
  /* eslint-disable */

  React.useEffect(() => {
    if (agreeOne && agreeTwo) {
      setEnableProceed(false);
    } else {
      setEnableProceed(true);
    }
  });

  React.useEffect(() => {
    props.clearUpdateFDAccount();
  }, []);

  React.useEffect(() => {
    console.log("props click", props.makePayment);
    if (
      props.makePayment &&
      props.makePayment.code === "URL" &&
      props.makePayment.data
    ) {
      console.log("url", props.makePayment.data);
      window.location.href = props.makePayment.data;
    }
  });

  return (
    <div className="DS">
      <AppBar name="Deposit Summary" />
      <div className="DSwrapper">
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <p className="appNoTxt">Application Number</p>
          </Grid>
          <Grid item xs={6}>
            <p className="appNoTxt algnRite">{props.summary.application_no}</p>
          </Grid>
        </Grid>
      </div>
      <div className="investmentAmountBox">
        <button className="editIcon" onClick={() => editFunc(props)}>
          Edit <EditIcon />
        </button>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <p className="numberTxt">
              <span>₹ </span>
              <span
                id="myP"
                maxLength="10"
                type="number"
                className={classes.editNum}
              >
                {props.summary.fd_amount}
              </span>
              {/* <button className="editIcon" onClick={() => editFunc(props)}>
                <EditIcon />
              </button> */}
            </p>
            <p className="liteGrayTxt">Investment Amount </p>
          </Grid>
          <Grid item xs={6}>
            <p className="numberTxt">
              <span>₹ </span>
              <span
                id="myP"
                maxLength="10"
                type="number"
                className={classes.editNum}
              >
                {props.summary.tenure}
              </span>
            </p>
            <p className="liteGrayTxt">{props.summary.scheme}</p>
          </Grid>
        </Grid>

        <Grid container spacing={3}>
          <Grid item xs={6}>
            <p className="darkBlk2">{props.summary.roi}% p.a.</p>
            <p className="liteGrayTxt">Effective Rate of Interest</p>
          </Grid>
          <Grid item xs={6}>
            <p className="purpleColor">Rs. {props.summary.mat_amount}</p>
            <p className="liteGrayTxt">Maturity Amount</p>
          </Grid>
        </Grid>
      </div>
      <div className="greyBg">
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <p className="darkBlk shortSentence">{props.summary.name}</p>
            <p className="liteGrayTxt">Depositor</p>
          </Grid>
          <Grid item xs={6}>
            <p className="darkBlk">
              {/* {props.personalDetails.addNomineeForm &&
              Object.keys(props.personalDetails.addNomineeForm).length > 0
                ? props.personalDetails.addNomineeForm.firstName +
                  " " +
                  props.personalDetails.addNomineeForm.lastName
                : "---"} */}
              {props.summary.nomineeName}
            </p>
            <p className="liteGrayTxt">Nominee</p>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <p className="darkBlk">{props.summary.pan}</p>
            <p className="liteGrayTxt">Pan Card</p>
          </Grid>
          <Grid item xs={6}>
            <p className="darkBlk">{props.summary.bankAccNo}</p>
            <p className="liteGrayTxt">A/C Number</p>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <p className="darkBlk">{props.summary.address}</p>
            <p className="liteGrayTxt">Address</p>
          </Grid>
        </Grid>
      </div>

      <div className="digitalPhysicalBtn">
        <p>How do you like to receive your fixed deposit?</p>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setRCVMode("D")}
          className={rcvMode === "D" ? "digitalPhysicalBtnActive" : ""}
        >
          Digital
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setRCVMode("P")}
          className={
            rcvMode === "P" ? "digitalPhysicalBtnActive floatRite" : "floatRite"
          }
        >
          Physical
        </Button>
      </div>

      <div className="greyBg checkWrapper">
        <p className="checkIconTXT">
          <Checkbox className="vkgs" id="aggree1" onClick={aggreeOne} />
          <small>
            I declare and authorize the company to process my online fixed
            deposit application without obtaining my physical signature on the
            application form.
          </small>
        </p>
        <p className="checkIconTXT">
          <Checkbox id="aggree2" onClick={aggreeTwo} />
          <small>I understand and agree to terms and conditions.</small>
        </p>

        <Button
          variant="contained"
          color="primary"
          disabled={enableProceed}
          onClick={proceedPayment}
        >
          Proceed To Pay
        </Button>
      </div>
    </div>
  );
};

function mapStateToProps(store) {
  return {
    summary: store.uploadDocuments.depositSummary,
    bankDetails: store.bankDetails,
    makePayment: store.uploadDocuments.makePayment,
    personalDetails: store.personalDetails
  };
}

const mapActionToProps = {
  summaryDetails,
  clearUpdateFDAccount,
  makePaymentAPI
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(DepositSummary));
