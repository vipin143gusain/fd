import {
  UPDATE_YEAR,
  UPDATE_PAUOUT_OPTION,
  UPDATE_AMOUNT,
  UPDATE_INTEREST,
  ENABLE_EDIT,
  UPDATE_ROI
} from "../constants";
const INITIAL_STATE = {
  year: 3,
  amount: 10000,
  total_interest: 0,
  roi: 9.1,
  payout_option: "MP",
  interesets: {
    MP: {
      "1": 9.1,
      "2": 9.6,
      "3": 10.1
    },
    QP: {
      "1": 9,
      "2": 9.5,
      "3": 10
    },
    is_enable: false
  },
  saveLeadId: false,
  updatedLead: false,
  updateFdAccount: false
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_YEAR:
      return { ...state, year: action.payload };
    case UPDATE_PAUOUT_OPTION:
      return { ...state, payout_option: action.payload };
    case UPDATE_AMOUNT:
      return { ...state, amount: action.payload };
    case UPDATE_INTEREST:
      return { ...state, total_interest: action.payload };
    case ENABLE_EDIT:
      return { ...state, is_enable: action.payload };
    case UPDATE_ROI:
      return { ...state, roi: action.payload };
    case "SAVED_LEAD":
      return { ...state, saveLeadId: action.payload.leadID };
    case "UPDATE_LEAD":
      return { ...state, updatedLead: action.payload.updatedLead };
    case "UPDATE_FD_ACCOUNT":
      return { ...state, updateFdAccount: action.payload.updateFdAccount };
    default:
      return state;
  }
};
