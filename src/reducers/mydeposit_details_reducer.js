const INITIAL_STATE = {
  deposit_details: []
};
export default (state = INITIAL_STATE, action) => {
  console.log("reducer", action);
  switch (action.type) {
    case "DEPOSIT_DETAILS":
      return {
        ...state,
        data: action.payload.data,
        totalAmount: action.payload.totalAmount,
        totalInterest: action.payload.totalInterest
      };
    case "EXISTING_CUSTOMER":
      return {
        ...state,
        customers: action.payload.customers
      };
    default:
      return state;
  }
};
