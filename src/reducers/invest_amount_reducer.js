const INITIAL_STATE = {
  updated_lead: false
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "UPDATE_LEAD":
      return { ...state, updated_lead: action.payload.updatedleadaction };
    default:
      return state;
  }
};
