const INITIAL_STATE = {
  result: "",
  depositSummary: "",
  documentList: {},
  storeDocumentList: false,
  makePayment: false,
  receiptPDF: false
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "RESPONSE_DOC_UPLOAD":
      return { ...state, result: action.payload.fieldValue };
    case "SUMMARY_DETAILS":
      return {
        ...state,
        depositSummary: action.payload.summaryDetails
      };
    case "DOCUMENT_LIST":
      return {
        ...state,
        documentList: action.payload
      };
    case "STORE_DOCUMENT_LIST":
      return {
        ...state,
        storeDocumentList: action.payload
      };
    case "MAKE_PAYMENT":
      return {
        ...state,
        makePayment: action.payload
      };
    case "CREATE_RECEIPT_PDF":
      return {
        ...state,
        receiptPDF: action.payload
      };
    default:
      return state;
  }
};
