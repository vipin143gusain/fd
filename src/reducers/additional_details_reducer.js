import {
  ADD_NOMINEE_FORM_ENABLE,
  FD_MINOR_FORM_ENABLE,
  JOINT_FD_FORM_ENABLE
} from "../constants";

const INITIAL_STATE = {
  additionalDetails: [],
  enableNomineeForm: false,
  enableMinorFdForm: false,
  enableJointFdForm: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_NOMINEE_FORM_ENABLE:
      return { ...state, enableNomineeForm: action.payload };
    case FD_MINOR_FORM_ENABLE:
      return { ...state, enableMinorFdForm: action.payload };
    case JOINT_FD_FORM_ENABLE:
      return { ...state, enableJointFdForm: action.payload };
    default:
      return state;
  }
};
