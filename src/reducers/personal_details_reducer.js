const INITIAL_STATE = {
  send_otp: false,
  verified_otp: false,
  close_modal: false,
  addNomineeForm: {},
  fdMinorForm: {},
  jointFdForm: {},
  jointFdForm2: {},
  saveOptional: {}
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SEND_OTP":
      return {
        ...state,
        send_otp: action.payload.fieldValue
      };
    case "UPDATE_FIELD_FORM1":
      return {
        ...state,
        verified_otp_form1: action.payload.fieldValue,
        close_modal_form1: true
      };
    case "UPDATE_FIELD_FORM2":
      return {
        ...state,
        verified_otp_form2: action.payload.fieldValue,
        close_modal_form2: true
      };
    case "ADD_NOMINEE_FORM":
      return {
        ...state,
        addNomineeForm: action.payload
      };
    case "FD_MINOR_FORM":
      return {
        ...state,
        fdMinorForm: action.payload
      };
    case "JOINT_FD_FORM":
      return {
        ...state,
        jointFdForm: action.payload
      };
    case "JOINT_FD_FORM2":
      return {
        ...state,
        jointFdForm2: action.payload
      };
    case "SAVE_OPTIONAL":
      return {
        ...state,
        saveOptional: action.payload
      };
    default:
      return state;
  }
};
