const INITIAL_STATE = {
  pan_number: "",
  is_loading: false,
  is_disable: true,
  dob: "",
  first_name: "",
  last_name: "",
  message: "",
  is_valid_pan: true,
  email: "",
  otp: "",
  is_otp_verified: false,
  openModal: true,
  invalid_otp: "",
  is_valid_email: false,
  addr_line1: "",
  addr_line2: "",
  pincode: "",
  city: "",
  state: "",
  proceed_to_additional: false,
  depositorsPanName: false,
  depositorsData: false,
  stateList: false,
  cityList: false,
  getStateName: false,
  getCityName: false
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_LOADING":
      return { ...state, is_loading: true };
    case "UPDATE_FIELD":
      return {
        ...state,
        [action.payload.fieldName]: action.payload.fieldValue
      };
    case "DEPOSITORS_PAN":
      return {
        ...state,
        depositorsPanName: action.payload.panName,
        depositorsData: action.payload.depositorsData,
        calledDepositorsPAN: action.payload.calledDepositorsPAN
      };
    case "UPDATE_DATA":
      return {
        ...state,
        is_loading: false,
        first_name: action.payload.first_name,
        last_name: action.payload.last_name,
        dob: action.payload.dob,
        is_disable: false,
        is_valid_pan: true
      };
    case "STATE_LIST":
      return {
        ...state,
        stateList: action.payload
      };
    case "CITY_LIST":
      return {
        ...state,
        cityList: action.payload
      };
    case "SET_STATE_NAME":
      return {
        ...state,
        getStateName: action.payload
      };
    case "SET_CITY_NAME":
      return {
        ...state,
        getCityName: action.payload
      };
    default:
      return state;
  }
};
