const INITIAL_STATE = {
  account_number: "",
  verify_account_number: "",
  ifsc_code: "",
  invalid_account: false,
  invalid_ifsc: false,
  invalid_verify_account: false,
  is_acc_verified: false,
  verifyBankData: false,
  verifyBankAccName: false,
  verifyBankMessage: false,
  ccNumber: false,
  saveCustomer: false
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_LOADING":
      return { ...state, is_loading: true };
    case "UPDATE_FIELD":
      return {
        ...state,
        [action.payload.fieldName]: action.payload.fieldValue
      };
    case "UPDATE_DATA":
      return {
        ...state,
        is_loading: false,
        first_name: action.payload.first_name,
        last_name: action.payload.last_name,
        dob: action.payload.dob,
        is_disable: false
      };
    case "VERIFY_BANK_ACCOUNT":
      return {
        ...state,
        verifyBankData: action.payload.verifyBankData,
        verifyBankAccName: action.payload.verifyBankAccName,
        verifyBankMessage: action.payload.verifyBankMessage,
        verifyBankStatus: action.payload.verifyBankStatus
      };
    case "NEXT_APP_NO":
      return {
        ...state,
        ccNumber: action.payload.ccNumber
      };
    case "SAVE_CUSTOMER":
      return {
        ...state,
        saveCustomer: action.payload.saveCustomer
      };
    default:
      return state;
  }
};
