import { combineReducers } from "redux";
import pancardReducer from "./pancard_reducer";
import bankDetailsReducer from "./bank_details_reducer";
import calculatorReducer from "./calculator_reducer";
import additionalDetailsReducer from "./additional_details_reducer";
import personalDetailsReducer from "./personal_details_reducer";
import investAmountReducer from "./invest_amount_reducer";
import uploadDocumentReducer from "./upload_documents_reducer";
import depositDetailsReducer from "./mydeposit_details_reducer";

export default combineReducers({
  pancard: pancardReducer,
  bankDetails: bankDetailsReducer,
  calculator: calculatorReducer,
  additionalDetails: additionalDetailsReducer,
  personalDetails: personalDetailsReducer,
  investAmountDetails: investAmountReducer,
  uploadDocuments: uploadDocumentReducer,
  depositDetails: depositDetailsReducer
});
