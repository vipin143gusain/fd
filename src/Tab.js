import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import CongratulationsIcon from "./images/congratulationsIcon.svg";
import Faq from "./Faq";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={event => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  bgPurple: {
    backgroundColor: "#e2e6f3",
    padding: "20px 0 60px",
    marginBottom: "40px"
  },
  faqPadding: {
    padding: "0 4px"
  }
}));

export default function NavTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          variant="fullWidth"
          value={value}
          onChange={handleChange}
          aria-label="nav tabs example"
        >
          <LinkTab label="Features" href="/drafts" {...a11yProps(0)} />
          <LinkTab label="How to Apply" href="/trash" {...a11yProps(1)} />
          <LinkTab label="FAQs" href="/spam" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel className="firstTab" value={value} index={0}>
        <p>Why Invest in </p>
        <p className="fdTxt">Future Group Fixed Deposit</p>

        <div className={classes.bgPurple}>
          <Grid container spacing={3}>
            <Grid item xs={12} className="tabTxt">
              <p className="benefitsTxt">Benefits of starting a new FD</p>
            </Grid>
            <Grid item xs={4} className="tabInside">
              <img src={CongratulationsIcon} alt="" />
              <p>High returns</p>
              <p className="lightTxt">Upto 8.5%</p>
            </Grid>
            <Grid item xs={4} className="tabInside">
              <img src={CongratulationsIcon} alt="" />
              <p>Zero commission</p>
              <p className="lightTxt">Quick and Easy</p>
            </Grid>
            <Grid item xs={4} className="tabInside">
              <img src={CongratulationsIcon} alt="" />
              <p>0.25% more Interest</p>
              <p className="lightTxt">for Senior Citizens</p>
            </Grid>
          </Grid>
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        Page Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Faq className="faqPadding" />
      </TabPanel>
    </div>
  );
}
