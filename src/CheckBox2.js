import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

export default function FormControlLabelPosition() {
  return (
    <FormControlLabel
      value="end"
      control={<Checkbox color="primary" />}
    //   label="End"
      labelPlacement="end"
    />
  );
}
