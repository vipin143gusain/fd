import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import AppBar from "./AppBar";
import GreenTickIcon from "./images/greenTickIcon.svg";
import ProcessingIcon from "./images/processingIcon.svg";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import MenuItem from "./MenuItems";
import JSZip from "jszip";
import {
  uploadDocumentCall,
  getDocumentList,
  storeDocumentList,
  makePaymentAPI
} from "./actions/upload_documents_action";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
// import { setTimeout } from "timers";
import queryString from "query-string";

let currencies = [];
var docMap = [];
var data = [
  {
    doc_name: "Aadhar Card ( Front & Back Side)",
    doc_codes: {
      back: "28",
      front: "3"
    }
  },
  {
    doc_name: "Driving License ( Front & Back Side)",
    doc_codes: {
      back: "29",
      front: "2"
    }
  },
  {
    doc_name: "Voter ID ( Front & Back Side)",
    doc_codes: {
      back: "30",
      front: "4"
    }
  },
  {
    doc_name: "Passport ( Front & Back Side)",
    doc_codes: {
      back: "27",
      front: "8"
    }
  },
  {
    doc_name: "Electricity Bill (Front Side)",
    doc_codes: {
      front: "9"
    }
  },
  {
    doc_name: "Phone Bill (Front Side)",
    doc_codes: {
      front: "10"
    }
  },
  {
    doc_name: "Bank Passbook (Fron & Back Side)",
    doc_codes: {
      back: "31",
      front: "11"
    }
  },
  {
    doc_name: "Rent Receipt ( Front Side)",
    doc_codes: {
      front: "12"
    }
  },
  {
    doc_name: "Leace And License Agreement( Front Side)",
    doc_codes: {
      front: "13"
    }
  }
];

var compressedDocuments = "";
var countOfSides = 0;

const UploadDocuments = props => {
  console.log("Prrrrr", props);
  const dispatch = useDispatch();
  const documentListSelector = useSelector(state => state.uploadDocuments); // Store Selector

  const [currenciesState, setCurrencies] = useState(currencies); // Loading state

  console.log(props, documentListSelector);
  var showBtn = true;
  const [files, setFiles] = useState([]);
  const [showImage, setImage] = useState(false);
  const [showBckImage, setBack] = useState(false);
  var parsed = queryString.parse(props.location.search);
  /* eslint-disable */
  /* eslint-disable */

  function onConfirm() {
    console.log("fail===================");
    let data = {
      type: "customer",
      application_no: props.match.params.Cn,
      pay_mode: "O",
      fd_rcv_mode: props.match.params.Mode,
      rdrUrl:
        "http://192.168.1.5:3000/upload-documents/" +
        props.match.params.Mode +
        "/" +
        props.match.params.Cn
    };
    console.log("props payment", props);
    props.makePaymentAPI(data);
  }

  React.useEffect(() => {
    console.log("props click", props.makePayment);
    if (
      props.makePayment &&
      props.makePayment.code === "URL" &&
      props.makePayment.data
    ) {
      console.log("url", props.makePayment.data);
      window.location.href = props.makePayment.data;
    }
  });

  React.useEffect(() => {
    dispatch(getDocumentList());
    console.log("vipi ========== ", props.match.params);
    if (parsed.paymentStatus === "failed") {
      window.confirm("Payment Failed. Do you want to Try Again?")
        ? onConfirm()
        : props.history.push("/deposit-summary/" + props.match.params.Cn);
    }
  }, []);

  React.useEffect(() => {
    if (
      documentListSelector &&
      Object.keys(documentListSelector.documentList).length > 0 &&
      !documentListSelector.storeDocumentList
    ) {
      documentListSelector.documentList.data.poa_list.map((key, index) => {
        currencies.push({
          value: index,
          label: key.doc_name
        });
      });
      setCurrencies(currencies);
      dispatch(storeDocumentList(true));
    }
  });

  const _onChange = event => {
    event.preventDefault();
    console.log("adding file ", event.target.files);
    setFiles(event.target.files);
    docMap["panCard"] = event.target.files[0].name;
  };

  const [files2, setFiles2] = useState([]);
  const _onChange2 = event => {
    setFiles2(event.target.files);
    docMap["proofOfAddress_Front"] = event.target.files[0].name;
  };

  const [files3, setFiles3] = useState([]);
  const _onChange3 = event => {
    setFiles3(event.target.files);
    docMap["proofOfAddress_Back"] = event.target.files[0].name;
  };

  function getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  const createCompressedFile = () => {
    // let imageData1 = "";
    // var imageData2 = false;
    // var imageData3 = false;
    // if (files.length > 0) {
    //   imageData1 = test(files[0]);
    //   imageData2 = getBase64(files[0]).then(data => {
    //     return data.replace("data:image/jpeg;base64,", "");
    //   });
    // }

    //Compressing images using jszip
    let zip = new JSZip();
    if (files && files.length > 0) {
      console.log("~~~~~VIPIN~~~~~~~", files);
      zip.file(files[0].name, files[0]); //filename & content
    }
    if (files2 && files2.length > 0) {
      zip.file(files2[0].name, files2[0]);
    }
    if (files3 && files3.length > 0) {
      zip.file(files3[0].name, files3[0]);
    }

    zip.generateAsync({ type: "blob" }).then(function(blob) {
      //compressedDocuments = new File([content], "myDocs_old.zip");
      console.log("-------compressedFile--------- ", props);

      var params = {
        application_no: props.match.params.Cn,
        type: "customer",
        token: "",
        docNames: [
          {
            name: docMap["panCard"],
            code: "1"
          },
          {
            name: docMap["proofOfAddress_Front"],
            code: docMap["code1"]
          }
        ]
      };
      if (showBckImage && params && params.docNames) {
        params.docNames.push({
          name: docMap["proofOfAddress_Back"],
          code: docMap["code2"]
        });
      }
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "multipart/form-data");

      var formdata = new FormData();
      //formdata.append("zipFile", compressedDocuments, "myDocs_old.zip");
      formdata.append("zipFile", blob);
      formdata.append("param", JSON.stringify(params));
      console.log("------- FormData -----", formdata);
      compressedDocuments = formdata;
      // var requestOptions = {
      //   method: "POST",
      //   headers: myHeaders,
      //   body: formdata,
      //   redirect: "follow"
      // };

      // fetch(
      //   "http://3.6.181.79:8080/fd_management/onboard/uploadDocuments",
      //   requestOptions
      // )
      //   .then(response => response.text())
      //   .then(result => console.log(result))
      //   .catch(error => console.log("error", error));

      ///////////////////////

      // $.ajax({
      //   data: formdata,
      //   url: "http://3.6.181.79:8080/fd_management/onboard/uploadDocuments",
      //   type: "POST",
      //   processData: false,
      //   contentType: false,
      //   mimeType: "multipart/form-data",
      //   success: function(response) {
      //     alert("success");
      //   }
      // });
    });
  };
  console.log(files);
  if (files && files.length > 0 && files2 && files2.length > 0) {
    if ((showBckImage && files3 && files3.length > 0) || !showBckImage) {
      showBtn = false;
    } else {
      showBtn = true;
    }
    var compressedObj = createCompressedFile();
  } else {
    showBtn = true;
  }

  function submitLater() {
    console.log("redirection----------");
    var url = "/congratulations/" + props.match.params.Cn;
    props.history.push(url);
  }

  function uploadDocumentsAPI() {
    //Make API call to upload data
    //prepare Param data
    // var params = {
    //   application_no: "CU001443",
    //   type: "customer",
    //   token: "",
    //   docNames: [
    //     {
    //       name: "pan.jpg",
    //       code: "1"
    //     },
    //     {
    //       code: "3",
    //       name: "POA.pdf"
    //     }
    //   ]
    // };
    console.log("A", compressedDocuments);
    //console.log("B", params);
    var success = props
      .uploadDocumentCall(compressedDocuments)
      .then(success => {
        console.log("chandan====", success);

        if (success) {
          submitLater();
        } else {
          window.alert("Upload Documents Failed. Please Try again");
        }
      });
  }

  function showDocumentList() {
    console.log("calllllingggg");
    return <MenuItem label="Select one" options={currenciesState} />;
  }

  function showOptions() {
    console.log("showOptions", currenciesState);
    return currenciesState.map(option => {
      return (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      );
    });
  }

  function cancel() {
    setFiles(null);
  }

  function cancel2() {
    setFiles2(null);
  }

  function cancel3() {
    setFiles3(null);
  }

  function handleOptionsChange(event) {
    console.log("Calling", event.target.value);
    console.log("data", data);
    var index = event.target.value;
    var val = index ? data[index] : {};
    console.log("Calling val", index === "");
    if (index === "") {
      console.log("default");
      setImage(false);
      return;
    } else {
      countOfSides = Object.keys(val.doc_codes).length;
      countOfSides > 0 ? setImage(true) : setImage(false);
      console.log("count", countOfSides);
      docMap["code1"] = val.doc_codes.front;
      if (val.doc_codes.back) {
        setBack(true);
        docMap["code2"] = val.doc_codes.back;
      } else {
        setBack(false);
        delete docMap["code2"];
      }
      console.log("docMap", docMap);
    }
  }

  return (
    <div className="uploadDocWrapper">
      <AppBar name="Upload Documents" />
      <div className="marTop">
        <div className="tickIconWrapper">
          <img className="greenTickIcon" src={GreenTickIcon} alt="" />
          <span className="checkboxFltLft">
            Payment succesful for Application
            <span className="checkboxFltLft">#{props.match.params.Cn}</span>
          </span>

          <div className="greyBg">
            <img className="processingIcon" src={ProcessingIcon} alt="" />

            <p className="processingTxt">
              We’re almost done. Upload Documents to complete Order processing.
            </p>
          </div>

          <div className="clearfix">
            <Typography className="instruction2 instruction" variant="h6">
              PAN card *
            </Typography>

            {/* <Typography className="MandatoryField" variant="h6">
              Upload documents failed
            </Typography> */}
          </div>
          <div>
            <form>
              <label className="custom-file-upload">
                <input
                  accept="image/jpeg, application/pdf"
                  type="file"
                  name="user[image]"
                  id="uploadImg"
                  onChange={_onChange}
                  onClick={event => {
                    event.target.value = null;
                  }}
                />
                Select File
              </label>
            </form>
          </div>

          <div className="uploadDocs">
            {/* <>
              <uploadDocsImg />
            </> */}
            {files &&
              [...files].map((file, index) =>
                file["type"] === "image/jpeg" ||
                file["type"] === "image/jpg" ||
                file["type"] === "image/pdf" ? (
                  <div>
                    <img
                      key={index}
                      alt={file["name"]}
                      className="uploadImagPreview"
                      src={URL.createObjectURL(file)}
                    />
                    <HighlightOffIcon
                      className="closeButton"
                      value="1"
                      onClick={cancel}
                    />
                  </div>
                ) : (
                  <div>
                    {file["name"]}
                    <HighlightOffIcon
                      className="closeButton"
                      value="1"
                      onClick={cancel}
                    />
                  </div>
                )
              )}
          </div>
          {/* {showFailed && (
            <div className="uploadFailedWrapper">
              <Typography
                className="uploadFailedTxt"
                variant="h6"
                disabled={true}
              >
                Upload documents failed
              </Typography>
            </div>
          )} */}

          {/* <Grid item xs={2}6787890-=
          ]lx Se51q `1`1  >
            </Grid>
            <Grid item xs={5} className="capture">
              <Button
                variant="contained"
                color="default"
                className={classes.button}
                startIcon={<CameraAltIcon />}
              >
                Capture
              </Button>
            </Grid> */}
        </div>

        <div className="greyBg">
          <p className="instruction2">Proof of Address (Select any 1)</p>
          {console.log("BBBBBB", currenciesState, currenciesState.length)}
          <select id="poa" onChange={handleOptionsChange}>
            <option value="">Select</option>
            {showOptions()}
          </select>

          {showImage && (
            <div className="proofAddWrapper" disabled>
              {/* {currenciesState &&
            currenciesState.length > 0 && ( // <MenuItem label="Select one" options={currenciesState} />
                //showDocumentList()
                <MenuItem label="Select one" options={currenciesState} />
              )} */}

              <div className="clearfix">
                <Typography className="instruction2 instruction" variant="h6">
                  Proof of Address (Front) *
                </Typography>
              </div>
              <Grid container spacing={1}>
                <Grid item xs={12} className="reqOTP">
                  <div>
                    <form>
                      <label className="custom-file-upload">
                        <input
                          accept="image/jpeg, application/pdf"
                          type="file"
                          id="uploadImg"
                          name="user[image]"
                          onChange={_onChange2}
                          onClick={event => {
                            event.target.value = null;
                          }}
                        />
                        Select File
                      </label>
                    </form>
                  </div>

                  <div className="uploadDocs">
                    {/* <>
                      <uploadDocsImg />
                    </> */}
                    {files2 &&
                      [...files2].map((file, index) =>
                        file["type"] === "image/jpeg" ||
                        file["type"] === "image/jpg" ||
                        file["type"] === "image/pdf" ? (
                          <div>
                            <img
                              key={index}
                              alt={file["name"]}
                              className="uploadImagPreview"
                              src={URL.createObjectURL(file)}
                            />
                            <HighlightOffIcon
                              className="closeButton"
                              onClick={cancel2}
                            />
                          </div>
                        ) : (
                          <div>
                            {file["name"]}
                            <HighlightOffIcon
                              className="closeButton"
                              onClick={cancel2}
                            />
                          </div>
                        )
                      )}
                  </div>
                </Grid>
              </Grid>
              {showBckImage && (
                <div>
                  <div className="clearfix">
                    <Typography
                      className="instruction2 instruction"
                      variant="h6"
                    >
                      Proof of Address (Back) *
                    </Typography>
                  </div>
                  <Grid container spacing={1}>
                    <Grid item xs={12} className="reqOTP">
                      <div>
                        <form>
                          <label class="custom-file-upload">
                            <input
                              accept="image/jpeg, application/pdf"
                              type="file"
                              name="user[image]"
                              id="uploadImg"
                              onChange={_onChange3}
                              onClick={event => {
                                event.target.value = null;
                              }}
                            />
                            Select File
                          </label>
                        </form>
                      </div>

                      <div className="uploadDocs">
                        {/* <>
                          <uploadDocsImg />
                        </> */}
                        {files3 &&
                          [...files3].map((file, index) =>
                            file["type"] === "image/jpeg" ||
                            file["type"] === "image/jpg" ||
                            file["type"] === "image/pdf" ? (
                              <div>
                                <img
                                  key={index}
                                  alt={file["name"]}
                                  className="uploadImagPreview"
                                  src={URL.createObjectURL(file)}
                                />
                                <HighlightOffIcon
                                  className="closeButton"
                                  onClick={cancel3}
                                />
                              </div>
                            ) : (
                              <div>
                                {file["name"]}
                                <HighlightOffIcon
                                  className="closeButton"
                                  onClick={cancel3}
                                />
                              </div>
                            )
                          )}
                      </div>
                    </Grid>
                  </Grid>
                </div>
              )}
            </div>
          )}
        </div>

        <div className="bottomBtn">
          <p>* Please upload documents upto 2 MB.</p>
          <Button
            variant="contained"
            disabled={showBtn}
            color={showBtn ? "primary" : "red"}
            className="uploadDocsBtn"
            onClick={uploadDocumentsAPI}
          >
            Upload Documents
          </Button>

          <Button
            onClick={submitLater}
            variant="contained"
            color="primary"
            className="submitBtn"
          >
            Submit later
          </Button>
        </div>
      </div>
    </div>
  );
};

function mapStateToProps(store) {
  console.log("Redux Store", store);
  return {
    bankDetails: store.bankDetails,
    makePayment: store.uploadDocuments.makePayment
  };
}

const mapActionToProps = {
  uploadDocumentCall,
  getDocumentList,
  storeDocumentList,
  makePaymentAPI
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(UploadDocuments));
