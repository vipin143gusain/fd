import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import BankDetails from "./BankDetails";
// import PanCard from "./PanCard";
import InvestAmount from "./InvestAmount";
import Faq from "./Faq";
import ConfirmationPersonalDetail from "./ConfirmationPersonalDetail";
import AdditionalDetails from "./AdditionalDetails";
import App from "./App";
import Feedback from "./Feedback";
import DepositSummary from "./DepositSummary";

// ReactDOM.render(<BankDetails />, document.getElementById('fixedDeposit'));
// ReactDOM.render(<App />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<InvestAmount />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<PanCard />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<Faq />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<ConfirmationPersonalDetail />, document.getElementById("fixedDeposit"));

export default class Routers extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/additional-details">
            <AdditionalDetails />
          </Route>
          <Route path="/deposit-summary">
            <DepositSummary />
          </Route>
          <Route path="/feedback">
            <Feedback />
          </Route>
          <Route path="/pancard">
            <App />
          </Route>
          <Route path="/bank-details">
            <BankDetails />
          </Route>
          <Route path="/pancard">
            <App />
          </Route>
          <Route path="/invest-amount">
            <InvestAmount />
          </Route>
          <Route path="/faq">
            <Faq />
          </Route>
          <Route path="/confirmation-personal-detail">
            <ConfirmationPersonalDetail />
          </Route>
        </Switch>
      </Router>
    );
  }
}
