import React from "react";
import "./App.css";
import Typography from "@material-ui/core/Typography";
import {
  createMuiTheme,
  responsiveFontSizes,
  ThemeProvider
} from "@material-ui/core/styles";

function App() {
  let theme = createMuiTheme();
  theme = responsiveFontSizes(theme);
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Typography className="instruction" variant="h6">
          *If depositor is minor, provide Guardian’s PAN and DOB.
        </Typography>
      </ThemeProvider>
    </div>
  );
}

export default App;
