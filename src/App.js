import React from "react";
import "./App.css";
import AppBar from "./AppBar";
import PanCard from "./PanCard";

function App() {
  return (
    <div className="App">
      <AppBar name="Enter PAN Details" />
      <PanCard />
    </div>
  );
}

export default App;
