import React from "react";
import DropDownContainer from "./DropDownContainer";

export default function SimpleExpansionPanel() {
  return (
    <div className="expansionWrapper">
      <p className="perferncesTxt">Set your preferences for your deposit</p>
      <DropDownContainer />
    </div>
  );
}
