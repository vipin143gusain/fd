import React from "react";
import TextField from "@material-ui/core/TextField";

const TextBox = props => {
  return (
    <TextField
      inputProps={{
        maxLength: 10
      }}
      // autoCapitalize
      className={"vkg"}
      required
      id="filled-full-width"
      style={{ marginTop: 30, marginBottom: 0 }}
      placeholder={props.placeholder}
      name={props.name}
      onChange={props.onChange}
      helperText=""
      fullWidth
      value={props.value}
      margin="normal"
      InputLabelProps={{
        shrink: true
      }}
      variant="filled"
    />
  );
};

export default TextBox;
