import React from "react";
import ReactDOM from "react-dom";
// import App from "./App";
import * as serviceWorker from "./serviceWorker";
// import BankDetails from "./BankDetails";
// import PanCard from "./PanCard";
// import InvestAmount from  "./InvestAmount"
// import Faq from "./Faq";
// import ConfirmationPersonalDetail from "./ConfirmationPersonalDetail";
// import AdditionalDetails from "./AdditionalDetails";
import Routers from "./Routers";

// ReactDOM.render(<BankDetails />, document.getElementById('fixedDeposit'));
// ReactDOM.render(<App />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<InvestAmount />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<PanCard />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<Faq />, document.getElementById("fixedDeposit"));
// ReactDOM.render(<ConfirmationPersonalDetail />, document.getElementById("fixedDeposit"));
ReactDOM.render(<Routers />, document.getElementById("fixedDeposit"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
