import React from "react";
import AppBar from "../AppBar";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";
import TextField from "@material-ui/core/TextField";

export default function TaxCorner() {
  const [state, setState] = React.useState({
    age: "",
    name: "hai"
  });

  const handleChange = name => event => {
    setState({
      ...state,
      [name]: event.target.value
    });
  };
  return (
    <div>
      <AppBar name="Tax Corner" />
      <div className="taxCornerWrapper marTop">
        <div className="yesNoBtnWrapper">
          <h3>15G/H Form</h3>
          <p className="taxActTxt">
            Whether assessed for tax under Income Tax Act 1961
          </p>

          <div className="digitalPhysicalBtn">
            <Button variant="contained" color="primary">
              Yes
            </Button>
            <Button variant="contained" color="primary" className="floatRite">
              No
            </Button>
            <div className="formSelect">
              <FormControl>
                <InputLabel shrink htmlFor="age-native-label-placeholder">
                  Latest assessment year for which assessed
                </InputLabel>
                <NativeSelect
                  value={state.age}
                  onChange={handleChange("age")}
                  inputProps={{
                    name: "age",
                    id: "age-native-label-placeholder"
                  }}
                >
                  <option value="">2019-20</option>
                  <option value={10}>2018-19</option>
                  <option value={20}>2017-18</option>
                  <option value={30}>2016-17</option>
                </NativeSelect>
              </FormControl>
            </div>
            <div className="textFieldDiv">
              <TextField
                id="filled-helperText"
                label="Estimated income earned on deposit(s)"
                defaultValue="Default Value"
                variant="filled"
              />

              <TextField
                className="inputHeightChange"
                id="filled-helperText"
                label="Estimated total income including estimated income earned on deposit(s)"
                defaultValue="Default Value"
                variant="filled"
              />
              <p className="detailFormTxt">
                Detail of Form No. 15G/ 15H other than this form filled during
                the current financial year.
              </p>
              <TextField
                id="filled-helperText"
                label="a. No. Forms Filled*"
                defaultValue="Default Value"
                variant="filled"
              />

              <TextField
                id="filled-helperText"
                label="b. No. Forms Filled*"
                defaultValue="Default Value"
                variant="filled"
              />
            </div>
          </div>
          <Button variant="contained" className="proceedButton" color="primary">
            Proceed
          </Button>
        </div>
      </div>
    </div>
  );
}
