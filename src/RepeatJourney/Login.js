import React, { useState } from "react";
import AppBar from "../AppBar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

const Login = props => {
  const [hideBtn, setHideBtn] = useState(true);

  return (
    <div>
      <AppBar name="Login" />
      <div className="login">
        <p>
          An OTP sent to your mobile number 9911991199, <br /> please verify to
          Login
        </p>

        <form className="loginInput" noValidate>
          <TextField
            onChange={e => {
              if (e.target.value.length === 6) {
                setHideBtn(false);
              }
            }}
            autoComplete="off"
            id="standard-basic"
            label="Enter OTP"
            type="tel"
            inputProps={{
              maxLength: 6
            }}
          />
          <Button className="resendOTPbtn" variant="contained">
            Resend OTP
          </Button>
        </form>
        <Button
          className="loginBtn"
          style={{ backgroundColor: hideBtn ? "#ddd" : "#eb6024" }}
          variant="contained"
          disabled={hideBtn}
        >
          Log In
        </Button>
      </div>
    </div>
  );
};
export default Login;
