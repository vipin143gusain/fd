import React, { useState } from "react";
import AppBar from "../../src/AppBar";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ShareIcon from "@material-ui/icons/Share";
import GetAppIcon from "@material-ui/icons/GetApp";
import FDsummaryTabs from "./FDsummaryTabs";
import * as moment from "moment";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import { connect } from "react-redux";
import { withRouter } from "react-router";

const FDsummary = props => {
  const [data, setData] = useState([]);

  React.useEffect(() => {
    console.log("my props", props);
    setData(props.depositDetails.data[props.match.params.index]);
    console.log(data);
  }, []);

  const getDate = date => {
    return moment(date, "DD/MM/YYYY").format("Do MMM YYYY");
  };

  return (
    <div>
      <AppBar name="FD Summary" />
      <div className="fdSummaryWrapper marTop">
        <Grid container>
          <Grid item xs={6}>
            <p className="depIDtxt">Deposit ID</p>
          </Grid>
          <Grid item xs={6}>
            <p className="depIDtxtRite">{data.DepositID}</p>
          </Grid>
          <Grid item xs={12}>
            <p className="maturityAmtTxt">₹ {data.MaturityAmount}</p>
            <p className="maturityAmtTxtDown">Maturity Amount </p>
          </Grid>
          <Grid item xs={5}>
            <p className="rateInterestTxt">{data.interest}% p.a.</p>
            <p className="maturityAmtTxtDown">Rate of Interest</p>
          </Grid>
          <Grid item xs={2}>
            <p className="hrLine"></p>
          </Grid>
          <Grid item xs={5}>
            <p className="rateInterestTxt">{getDate(data.MaturityDate)}</p>
            <p className="maturityAmtTxtDown">Maturity Date</p>
          </Grid>
        </Grid>

        <div className="showMoreDiv">
          <ExpansionPanel>
            <ExpansionPanelSummary
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <p className="heading">Show More</p>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container>
                <Grid item xs={6}>
                  <p className="rateInterestTxt">{data.DepositorName}</p>
                  <p className="maturityAmtTxtDown">Primary Holder</p>
                </Grid>

                <Grid item xs={6}>
                  <p className="rateInterestTxt">₹ {data.DepositAmount}</p>
                  <p className="maturityAmtTxtDown">Principal Amount</p>
                </Grid>

                {/* <Grid item xs={6}>
                  <p className="rateInterestTxt">Ram Kumar</p>
                  <p className="maturityAmtTxtDown">Nominee</p>
                </Grid> */}

                <Grid item xs={6}>
                  <p className="rateInterestTxt">{getDate(data.DepositDate)}</p>
                  <p className="maturityAmtTxtDown">Start Date</p>
                </Grid>

                {/* <Grid item xs={6}>
                  <p className="rateInterestTxt">12 Months 1 Day</p>
                  <p className="maturityAmtTxtDown">Duration</p>
                </Grid> */}

                <Grid item xs={6}>
                  <p className="rateInterestTxt redTxt">Not Availed</p>
                  <p className="maturityAmtTxtDown">NA</p>
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>

        <div className="buttonShareDownload">
          <Button variant="contained">
            Share
            <ShareIcon />
          </Button>
          <Button variant="contained" className="floatRite">
            Download
            <GetAppIcon />
          </Button>
        </div>

        <FDsummaryTabs />
      </div>
    </div>
  );
};

function mapStateToProps(store) {
  console.log("Redux Store", store);
  return {
    depositDetails: store.depositDetails
  };
}

const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(FDsummary));
