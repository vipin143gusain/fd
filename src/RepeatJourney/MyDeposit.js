import React, { useState } from "react";
import AppBar from "../AppBar";
import MyDepositIcon from "../images/myDeposit/myDepositIcon.svg";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import SlickCarousel from "../SlickCarousel";
import CallIcon from "../images/myDeposit/callIcon.svg";
import TaxCornerIcon from "../images/myDeposit/taxCornerIcon.svg";
import { fetchDepositDetails } from "../actions/mydeposit_details_action";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import * as moment from "moment";
import { Link } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import OpenNewFDBTN from "../RepeatJourney/MyDepositModal";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(0.8),
      width: theme.spacing(46),
      height: theme.spacing(10)
    }
  }
}));

const MyDeposit = props => {
  const classes = useStyles();
  const [showLoader, setLoader] = useState(true);

  const getDate = date => {
    return moment(date, "DD/MM/YYYY").format("Do MMMM YYYY");
  };

  React.useEffect(
    () => {
      console.log("after optional props", props);
      props.fetchDepositDetails("9987428022");
    } /* eslint-disable */,
    []
  );

  React.useEffect(() => {
    console.log("after optional props", props);
    if (props.depositDetails.data) {
      setLoader(false);
    }
  });

  return (
    <div>
      {showLoader && <CircularProgress className="progressBar" />}
      <AppBar />

      {!showLoader && (
        <div className="myDepositWrapper">
          <div className="myDepositBg">
            <div className="bgIcon">
              <img src={MyDepositIcon} alt="asdf" />
              <p>My Deposits</p>
              <div className="twoBoxes">
                <div className="boxOne">
                  <p>₹ {props.depositDetails.totalAmount}</p>
                  <p className="lightTxt">Total deposits</p>
                </div>

                <div className="boxOne helpIcon">
                  <p className="orangeColor">
                    ₹
                    {props.depositDetails.totalAmount +
                      props.depositDetails.totalInterest}
                  </p>
                  <p className="lightTxt">Maturity Value</p>
                </div>
              </div>

              {/* <Paper elevation={1} className="pendingKYC">
                <p>₹ 2,34,687</p>
                <p className="pendingTxt">PENDING</p>
                <div className="maturityTxtBox">
                  <div className="leftTxt">! Document Upload & KYC Pending</div>
                  <div className="riteIcon">
                    <Link
                      className="linkColorChange"
                      to="/upload-documents/:mode/:Cn"
                    >
                      <ArrowForwardIosIcon />
                    </Link>
                  </div>
                </div>
              </Paper> */}
            </div>
          </div>

          <div className="greyBg paddingAll">
            <div className={classes.root}>
              {props.depositDetails.data &&
                props.depositDetails.data.map((data, index) =>
                  index < 5 ? (
                    <div className="vkgg">
                      <div className={classes.root}>
                        <Paper elevation={1} key={index}>
                          <p>₹ {data.DepositAmount}</p>
                          <div className="maturityTxtBox">
                            <div className="leftTxt">
                              Maturing on&nbsp;{getDate(data.MaturityDate)}
                            </div>
                            <div className="riteIcon">
                              <Link
                                className="linkColorChange"
                                to={"/fd-summary/" + index}
                              >
                                <ArrowForwardIosIcon />
                              </Link>
                            </div>
                          </div>
                        </Paper>
                      </div>
                    </div>
                  ) : null
                )}

              <OpenNewFDBTN />

              <div className="linkWrapper">
                <div className="linkDiv borderTop">
                  <p>
                    <img src={TaxCornerIcon} alt="Tax Corner" />
                    Tax Corner
                  </p>
                </div>

                <div className="linkDiv">
                  <p>
                    <img src={CallIcon} alt="Contact Us" />
                    Contact Us
                  </p>
                </div>
              </div>
              <div className="carousalWrapper">
                <SlickCarousel />
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

function mapStateToProps(store) {
  console.log("Redux Store", store);
  return {
    depositDetails: store.depositDetails
  };
}

const mapActionToProps = {
  fetchDepositDetails
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(MyDeposit));
