import React from "react";
import AppBar from "../AppBar";
import Grid from "@material-ui/core/Grid";

export default function Preconfirmation() {
  return (
    <div>
      <AppBar name="Pre-confirmation" />
      <div className="preconfirmationWrapper marTop">
        <p className="verifyTxt">Verify and confirm details</p>
        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>Account Number</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">071901509134</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>PAN Number</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">BWXPS8688N</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>Date of birth</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">19-10-1986</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>Status</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">Individual</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>Assessed for Tax under Income Tax Act 1961</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">Yes</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>Latest assessment year for which assessed</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">2019-20</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>Estimated income earned on deposit(s)</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">₹ 2,34,687</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>
              Estimated total income including the estimated income earned on
              deposit(s)
            </p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">₹ 2,34,687</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>No. of Forms</p>
          </Grid>
          <Grid item xs={5}>
            <p>0</p>
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={7}>
            <p>Aggregate Amount</p>
          </Grid>
          <Grid item xs={5}>
            <p className="darkTxt">₹ 0.00</p>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
