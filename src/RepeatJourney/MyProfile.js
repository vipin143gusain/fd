import React from "react";
import AppBar from "../AppBar";
import Grid from "@material-ui/core/Grid";
import CheckCircleIcon2 from "../images/myDeposit/stepperActiveIcon.svg";

export default function MyDeposite() {
  return (
    <div>
      <AppBar />
      <div className="resumeFDWrapper">
        <AppBar name="My Profile" />

        <div className="myDepositBg">
          <div className="bgIcon">
            <p className="resumeTxt">Vipin Kumar Gusain</p>
          </div>
        </div>

        <div className="greyBg paddingAll">
          <Grid container className="marBtm100">
            <Grid item xs={5}>
              <p className="profileColorChange">Number</p>
            </Grid>
            <Grid item xs={7}>
              <p className="darkTxt2">APGPG6900K</p>
            </Grid>
            <Grid item xs={5}>
              <p className="profileColorChange">PAN</p>
            </Grid>
            <Grid item xs={7}>
              <p className="darkTxt2">BWXXXXXX4R</p>
            </Grid>
            <Grid item xs={12}>
              <img src={CheckCircleIcon2} className="checkIcon" alt="" />

              <p className="panVerfiedTxt">KYC Verification Done.</p>
            </Grid>
            <Grid item xs={5}>
              <p className="profileColorChange">E-mail</p>
            </Grid>
            <Grid item xs={7}>
              <p className="darkTxt2">RXXXXXXXXX98@gmail.com</p>
            </Grid>
            <Grid item xs={5}>
              <p className="profileColorChange">Address</p>
            </Grid>
            <Grid item xs={7}>
              <p className="darkTxt2">
                Joseph Chemmanur Hall, 1st Cross Rd, Kalyan Nagar, Indira Nagar
                1st Stage, H Colony, Indiranagar, Bengaluru, Karnataka 560038
              </p>
            </Grid>
          </Grid>

          <button className="continue">Continue</button>
        </div>
      </div>
    </div>
  );
}
