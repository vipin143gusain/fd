import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import ControlPointIcon from "@material-ui/icons/ControlPoint";
import Button from "@material-ui/core/Button";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import { fetchCustomerDetails } from "../actions/mydeposit_details_action";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import CloseIcon from "@material-ui/icons/Close";
const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: "10px",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: "76%",
    outline: "none"
  }
}));

const TransitionsModal = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const redirect = () => {
    props.history.push("/invest-amount/nil/true", {
      user: props.depositDetails.customers[value]
    });
  };

  React.useEffect(() => {
    if (!props.openModal) {
      setOpen(false);
    }
  }, [props.openModal]);

  React.useEffect(() => {
    console.log("calling api");
    props.fetchCustomerDetails("0123456789");
  }, []);

  React.useEffect(() => {
    console.log("after optional props", props);
    if (props.customerDetails && props.customerDetails.customers) {
      console.log("1234567", props.customerDetails.customers);
    }
  });

  const handleOpen = () => {
    // props.sendOTP(props.form);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [value, setValue] = React.useState(0);

  const handleChange = event => {
    console.log("-----event", event.target.value);
    console.log("-----typoe if event", typeof event.target.value);
    setValue(parseInt(event.target.value));
  };

  return (
    <div className="myDepositeBtnWrper">
      <Button
        className="openFDbtn"
        variant="contained"
        type="button"
        onClick={handleOpen}
        color="primary"
        // disabled={!props.isValidPhone || props.disabled}
      >
        <ControlPointIcon />
        Open New FD (9.25%)
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        disableBackdropClick={true}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <CloseIcon className="cloaseBtnIcon" onClick={handleClose} />
            <h2 id="transition-modal-title">
              {props.heading || "Select Profile"}
            </h2>
            <p id="transition-modal-description">
              You have multiple PAN Cards saved. Select one to Proceed.
            </p>

            <div className="scrollBox">
              <FormControl component="fieldset">
                <RadioGroup
                  aria-label="gender"
                  name="gender1"
                  value={value}
                  onChange={handleChange}
                >
                  {props.depositDetails.customers &&
                    props.depositDetails.customers.map((data, index) => (
                      <div className="selectPanCard">
                        <FormControlLabel
                          className="radioBtnColor"
                          value={index}
                          control={<Radio />}
                          label={data.PANNo}
                        />
                        <p className="panHolderName">{data.DepositorName}</p>
                      </div>
                    ))}
                </RadioGroup>
              </FormControl>
            </div>

            <Button
              variant="contained"
              className="ConfirmBtnMyDepositeModal"
              onClick={redirect}
            >
              Confirm
            </Button>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

function mapStateToProps(store) {
  console.log("Redux Store", store);
  return {
    depositDetails: store.depositDetails
  };
}

const mapActionToProps = {
  fetchCustomerDetails
};

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(TransitionsModal));
