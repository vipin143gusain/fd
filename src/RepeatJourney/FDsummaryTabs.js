import React from "react";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import { Link } from "react-router-dom";

export default function FDsummaryTabs() {
  return (
    <div className="fdTabWrapper">
      <div className="greyBg">
        <div className="tabOne">
          <p>Get Deposit Advice</p>
          <p className="arrowImg">
            <ArrowForwardIosIcon />
          </p>
        </div>
        <div className="tabOne">
          <p>Replicate This Deposit</p>
          <p className="arrowImg">
            <ArrowForwardIosIcon />
          </p>
        </div>
        <div className="tabOne">
          <p>Break this deposit</p>
          <p className="arrowImg">
            <ArrowForwardIosIcon />
          </p>
        </div>
        <div className="tabOne">
          <p>Open New Fixed Deposit (9.25%)</p>
          <p className="arrowImg">
            <ArrowForwardIosIcon />
          </p>
        </div>
        <div className="tabOne">
          <p>Update Nominee Details</p>
          <p className="arrowImg">
            <ArrowForwardIosIcon />
          </p>
        </div>
        <div className="tabOne">
          <p>List Down Other FD Actions</p>
          <p className="arrowImg">
            <ArrowForwardIosIcon />
          </p>
        </div>

        <div className="tabOne">
          <p>Give Feedback</p>
          <p className="arrowImg">
            <ArrowForwardIosIcon />
          </p>
        </div>

        <div className="tabOne">
          <Link to="/faq">
            <p>Help & FAQs</p>

            <p className="arrowImg">
              <ArrowForwardIosIcon />
            </p>
          </Link>
        </div>
      </div>
    </div>
  );
}
