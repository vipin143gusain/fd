import React from "react";
import AppBar from "../AppBar";
import EditIcon from "@material-ui/icons/Edit";

export default function MyDeposite() {
  return (
    <div>
      <AppBar />
      <div className="resumeFDWrapper">
        <AppBar name="Resume FD" />

        <div className="myDepositBg">
          <div className="bgIcon">
            <p className="resumeText">
              Your are almost done with your FD application. Resume application
              to complete investment.
            </p>
            <div className="whiteBox">
              <div className="leftBox">
                <p>Principal Value</p>
              </div>
              <div className="riteBox">
                <p className="textAlignRite">
                  ₹ 1,00,000 <EditIcon />
                </p>
              </div>

              <div className="leftBox">
                <p>Cumulative payouts</p>
              </div>
              <div className="leftBox">
                <p className="textAlignRite">Tenure : 3 yrs</p>
              </div>
            </div>
          </div>
        </div>

        <div className="greyBg paddingAll">
          <div class="stepper">
            <div class="step completed">
              <div class="v-stepper">
                <div class="circle"></div>
                <div class="line"></div>
              </div>

              <div class="content">
                <p>Personal Details</p>
                <p className="stepperColorChange">
                  (Verify PAN and contact details)
                </p>
              </div>
            </div>

            <div class="step active">
              <div class="v-stepper">
                <div class="circle"></div>
                <div class="line"></div>
              </div>

              <div class="content">
                <p>Bank Details</p>
                <p className="stepperColorChange">
                  (Verify account for payouts)
                </p>
              </div>
            </div>

            <div class="step active">
              <div class="v-stepper">
                <div class="circle"></div>
                <div class="line"></div>
              </div>

              <div class="content">
                <p>Make Payment</p>
                <p className="stepperColorChange">(Complete payment)</p>
              </div>
            </div>

            <div class="step active">
              <div class="v-stepper">
                <div class="circle"></div>
                <div class="line"></div>
              </div>

              <div class="content">
                <p>Upload Documents</p>
                {/* <p className="stepperColorChange">(Verify PAN and contact details)</p> */}
              </div>
            </div>

            <div class="step">
              <div class="v-stepper">
                <div class="circle"></div>
                <div class="line"></div>
              </div>

              <div class="content">
                <p>Deposit Secured</p>
                {/* <p className="stepperColorChange">(Verify PAN and contact details)</p> */}
              </div>
            </div>
          </div>

          <button className="continue">Continue</button>
        </div>
      </div>
    </div>
  );
}
