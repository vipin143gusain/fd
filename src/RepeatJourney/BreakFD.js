import React from "react";
import AppBar from "../AppBar";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Checkbox from "@material-ui/core/Checkbox";

export default function BreakFD() {
  const [checked, setChecked] = React.useState(true);

  const handleChange = event => {
    setChecked(event.target.checked);
  };
  return (
    <div>
      <AppBar name="Break FD" />
      <div className="BreakfdWrapper marTop">
        <Grid container>
          <Grid item xs={6}>
            <p className="depIDtxt">Deposit ID</p>
          </Grid>
          <Grid item xs={6}>
            <p className="depIDtxtRite">50300387823511</p>
          </Grid>
          <Grid item xs={12}>
            <p className="maturityAmtTxt">₹ 3,05,439</p>
            <p className="maturityAmtTxtDown">Maturity Amount </p>
          </Grid>

          <Grid item xs={12}>
            <p className="maturityAmtTxtDown">Transfer to</p>
            <p className="accTxt">A/C : 50300387823511 </p>
            <p className="accTxt">IFSC: ICIC0000419</p>
          </Grid>
        </Grid>

        <Grid container className="greyBg">
          <Grid item xs={6}>
            <p className="rateInterestTxt">₹ 2,86,449</p>
            <p className="maturityAmtTxtDown">Principal Amount</p>
          </Grid>

          <Grid item xs={6}>
            <p className="rateInterestTxt">04 Nov, 2019</p>
            <p className="maturityAmtTxtDown">Interest Earned</p>
          </Grid>

          <Grid item xs={6}>
            <p className="rateInterestTxt">12 Months 1 Day</p>
            <p className="maturityAmtTxtDown">Penalty Charges</p>
          </Grid>

          <Grid item xs={6}>
            <p className="rateInterestTxt">Declared</p>
            <p className="maturityAmtTxtDown">Lower Interest</p>
          </Grid>
        </Grid>

        <div className="tnc">
          <Checkbox
            checked={checked}
            onChange={handleChange}
            value="primary"
            inputProps={{ "aria-label": "primary checkbox" }}
          />
          <p>
            I Accept the
            <Link To="" className="orangeTxt">
              Terms & Conditions
            </Link>
          </p>
        </div>

        <div className="buttonShareDownload">
          <Button variant="contained">Cancel</Button>
        </div>
        <div className="buttonShareDownload">
          <Button variant="contained" className="">
            Confirm
          </Button>
        </div>
      </div>
    </div>
  );
}
