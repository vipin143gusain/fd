import React from "react";
import AppBar from "../AppBar";
import Button from "@material-ui/core/Button";
import ContactUsIcon from "../images/myDeposit/contactUsIcon.svg";
import EmailIcon from "@material-ui/icons/Email";
import CallIcon from "@material-ui/icons/Call";
export default function ContactUs() {
  return (
    <div>
      <AppBar name="Contact Us" />
      <div className="contactUsWrapper marTop">
        <img src={ContactUsIcon} alt="Contact Us Icon" />
        <p>
          Don’t worry! Our customer support crew are here to help you out.
          Contact us through your preferred options below.
        </p>

        <Button className="openFDbtn" variant="contained">
          <EmailIcon />
          Email Us
        </Button>
        <p className="greyTXT">
          From registered email-id jaoijafoijdvoksvo@gmail.com
        </p>

        <Button className="openFDbtn" variant="contained">
          <CallIcon />
          Call our Folks
        </Button>
        <p className="greyTXT">From registered number +91 8392835710</p>
      </div>
    </div>
  );
}
