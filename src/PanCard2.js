import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Calender from "./Calender";
import moment from "moment";
import axios from "axios";
import Instruction from "./Instruction";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: "65px"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  }
}));

export default function LayoutTextFields() {
  const classes = useStyles();

  const [pan, setPan] = useState("");
  const [apiResponse, setResponse] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [isDisable, setDisabled] = useState(true);

  let dateSelect = date => {
    setLoading(true);
    let dob = moment(date).format("DD/MM/YYYY");
    let postData = {
      pan: pan,
      dob: dob
    };
    axios
      .post(
        "http://13.126.20.61:8080/fd_management/verify/getPanDetails",
        postData
      )
      .then(response => {
        console.log(response.data);
        setResponse(response.data);
        setLoading(false);
        if (response.data.message !== "Invalid PAN") {
          setDisabled(false);
        }
      });
  };

  return (
    <div className={classes.container}>
      <div className="panCardWrapper">
        <TextField
          id="standard-basic"
          label="PAN Number"
          inputProps={{
            maxLength: 10
          }}
          className={"vkg"}
          fullWidth
          onChange={e => {
            let newPan = e.target.value;
            setPan(newPan);
          }}
          style={{ marginTop: 30 }}
        />

        {apiResponse && (
          <div className="verifiedMsg">{apiResponse.message}</div>
        )}
      </div>
      <Calender onSelect={dateSelect} />
      <Instruction />
      {!isLoading && (
        <Button
          variant="contained"
          disabled={isDisable}
          color="primary"
          className="proceedButton"
        >
          Proceed
        </Button>
      )}
      {isLoading && <CircularProgress className="progressBar" />}
    </div>
  );
}
