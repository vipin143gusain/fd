import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
// import { verify } from "crypto";
const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: "65px"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  }
}));

var invalid_verify_account = false;

function verifyAcc() {
  var accNo = document.getElementById("acc").value;
  var reAccNo = document.getElementById("reacc").value;
  console.log("asgjffgazkfk", accNo, reAccNo);
  if (accNo !== reAccNo) {
    invalid_verify_account = true;
    return;
  }
  invalid_verify_account = false;
}

const LayoutTextFields = props => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className="panCardWrapper">
        <TextField
          data-unselectable="unselectable content"
          autoComplete="off"
          inputProps={{
            maxLength: 15
          }}
          type="tel"
          className={"vkg"}
          required
          id="acc"
          style={{ marginTop: 30, marginBottom: 0 }}
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true
          }}
          variant="filled"
          placeholder="Account Number"
          onChange={e => {
            let accNo = e.target.value;
            props.updateData("account_number", accNo);
          }}
        />
        {props.invalid_account && (
          <div className="invalidMsg">Invalid Account Number</div>
        )}
        <TextField
          autoComplete="off"
          inputProps={{
            maxLength: 15
          }}
          type="tel"
          className={"vkg"}
          required
          id="reacc"
          style={{ marginTop: 30, marginBottom: 0 }}
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true
          }}
          variant="filled"
          placeholder="Re-enter Account Number"
          onChange={e => {
            let accNo = e.target.value;
            props.updateData("verify_account_number", accNo);
            verifyAcc();
          }}
        />
        {invalid_verify_account && (
          <div className="invalidMsg">Account Number Not matched</div>
        )}

        <TextField
          autoComplete="off"
          inputProps={{
            maxLength: 15
          }}
          className={"vkg"}
          required
          id="filled-full-width"
          style={{ marginTop: 30, marginBottom: 0 }}
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true
          }}
          variant="filled"
          placeholder="IFSC Number"
          onChange={e => {
            let ifsc = e.target.value;
            props.updateData("ifsc_code", ifsc);
          }}
        />
        {props.invalid_ifsc && <div className="invalidMsg">Invalid IFSC</div>}
      </div>
    </div>
  );
};

export default LayoutTextFields;
