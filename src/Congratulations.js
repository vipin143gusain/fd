import React from "react";
import AppBar from "./AppBar";
import Button from "@material-ui/core/Button";
import elephantIcon from "./images/congratulationsIcon.svg";
import SlickCarousel from "./SlickCarousel";
import GetAppSharpIcon from "@material-ui/icons/GetAppSharp";
import { createReceiptPDF } from "./actions/upload_documents_action";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";

const Congratulations = props => {
  const downloadReceipt = () => {
    let data = {
      type: "customer",
      // application_no: props.bankDetails.ccNumber,
      application_no: props.match.params.Cn,

      sendMail: false
    };
    props.createReceiptPDF(data);
  };

  return (
    <div>
      <AppBar name="Congratulations" />
      <div className="congratulationsWrapper">
        <div className="bannerBox">
          <img src={elephantIcon} alt="" />
          <h2>
            Thank you <br />
            for choosing Future Group <br />
            Fixed Deposits Scheme!
          </h2>
        </div>
        <div className="centerDiv">
          <div className="downlaodReceipt">
            <p>
              Your FD will be issued within 2 working days, subject to document
              verification and fund realization.
            </p>
            <Button
              className="DownloadReciptBtn"
              variant="contained"
              color="primary"
              onClick={downloadReceipt}
            >
              <GetAppSharpIcon fontSize="small" />
              Download Receipt
            </Button>
            <p>
              An electronic copy of the reciept will be sent to your registered
              e-mail ID.
            </p>
          </div>

          <div className="carousalWrapper">
            <SlickCarousel />
          </div>
        </div>

        <div className="linkDiv">
          <p>Open another FD</p>
        </div>

        <div className="linkDiv">
          <p>
            <Link to="/my-deposit">Fixed deposits overview</Link>
          </p>
        </div>

        <div className="linkDiv">
          <p>Go to Home</p>
        </div>
      </div>
    </div>
  );
};

function mapStateToProps(store) {
  console.log("Redux Store", store);
  return {
    bankDetails: store.bankDetails,
    uploadDocuments: store.uploadDocuments
  };
}

const mapActionToProps = { createReceiptPDF };

export default connect(
  mapStateToProps,
  mapActionToProps
)(withRouter(Congratulations));
