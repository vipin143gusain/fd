import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { purple } from "@material-ui/core/colors";

import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: "10px",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: "76%",
    outline: "none"
  }
}));

export default function TransitionsModal(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
    if (!props.openModal) {
      setOpen(false);
    }
  }, [props.openModal]);

  const handleOpen = () => {
    props.sendOTP(props.form);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const ColorButton = withStyles(theme => ({
    root: {
      color: theme.palette.getContrastText(purple[500]),
      backgroundColor: "#7d6498",
      "&:hover": {
        backgroundColor: "#7d6498"
      },
      margin: "0px",
      width: "100%",
      fontSize: "11px",
      fontFamily: "Roboto",
      fontWeight: "500",
      padding: "10px"
    }
  }))(Button);
  return (
    <div>
      <ColorButton
        className="sendVerfiyBtn reqOTP"
        type="button"
        onClick={handleOpen}
        variant="contained"
        color="primary"
        disabled={!props.isValidPhone || props.disabled}
      >
        Request OTP
      </ColorButton>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        disableBackdropClick={true}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">
              {props.heading || "Verify Email"}
            </h2>
            <p id="transition-modal-description">
              {`Verification ${props.mobileText} sent to ${props.para}`}
            </p>
            <p className="verificationCodeTxt">
              {props.verification || "Enter 6 digit verification code"}
            </p>

            <form className="pinTxtFielde" noValidate autoComplete="off">
              <TextField
                autoComplete="off"
                type="tel"
                id="standard-basic"
                inputProps={{
                  maxLength: 6
                }}
                onChange={e => {
                  let otp = e.target.value;
                  props.hanldeOTPText(otp);
                }}
              />
            </form>
            <div className="otpNotMatchedTxt"> {props.invalidOTP}</div>

            <div
              className={classes.root}
              style={{ textAlign: "right", margin: "39px 0 0" }}
            >
              <Button onClick={() => props.sendOTP(props.form)}>Resend</Button>
              <Button
                className="ConfirmBtn"
                href="#text-buttons"
                onClick={() => props.verifyOTP(props.form)}
              >
                Confirm
              </Button>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
